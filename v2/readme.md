# Installation

All scripts rely on the 'Base Class' and must be installed first. Calls to ServiceNow tables are done via the 'DB Class'.    

## Base class
script_includes > OO2Object.js

## DB class
script_includes > OO2Db.js




# DB Field Mapping

Fields to be mapped as part of the return object


Return specfic fields
```javascript
var m = "sys_id, user_name";
// Or pass an array
var m = ['sys_id', "email"];
```


Return all fields
```javascript
var m = "*";
```


Return a GlideRecord object. Use as normal i.e while(gr.next())...
```javascript
var m = true; // boolean true||false
```


Advance object
```javascript
var m = {
	"sys_id" 		: "sys_id", 	// get field
	"depart"    	: "$department", 	// get display value
	"department"   	: "department.description", 	// dot walk
	"boolean" 		: "=true", 		// set boolean
	"number" 		: "=1234",		// set number
	"short" 		: "@Email address ${email}", // set string, and allows for literals from the record
	"description" 	: (function(gr){ // callback on field
		// transform from glide
		return [gr.name, gr.user_name, gr.email].join(", ")
	}),
	// build multi-dimensional object
	"manager.email"			: "manager.email", 		// dot walk and build object
	"manager.user_name"		: "manager.user_name", 	// dot walk
	"manager.name.full"	 	: "$manager", 			// display value
	"manager.name.first" 	: "manager.first_name", // dot walk
	"manager.name.last" 	: "manager.last_name", 	// dot walk
}
```


## Query

DB call with passing mappings
```javascript
// Return Mapped Object
var o = new OO2Db("sys_user").query("sys_id="+ gs.getUserID(), m);

// Return Mapped Dictionary Elements
var o = new OO2Db("sys_user").query("sys_id="+ gs.getUserID(), m, true);
```


https://www.makeareadme.com/