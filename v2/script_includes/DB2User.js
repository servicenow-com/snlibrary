var DB2User = Class.create();
DB2User.prototype = Object.extendsObject(OO2Model, {

    /*
    * Extended DB Model class which sits on top of a table.
    * and adds a layer on top which can include some logic.     
    *
    * @author  Scott Ladyman
    * @version 2.0 (OO2), 06/2023
    *
    * OO2Object 
    *   |
    *   OO2Db 
    *       | 
    *       OO2Model
    *            |
    *            DB2User
    *   
    */

    // REQUIRED
    o : {},
    
    initialize: function(mappings, elements) 
    {
        OO2Model.prototype.initialize.call(this, {
            "table"     : "sys_user",
            // return the GlideRecord Object or mapped fields
            "mappings"  : mappings || true,
            // return SN field elements
            "elements"  : elements || false,            
        });       

    },


    /*
        Get all active users off the table
    */
    active : function(query)
    {
        return this.query("active=true"+ (query ? "^"+ query : ""))
    },

    type: 'DB2User'
});