var DB2Company = Class.create();
DB2Company.prototype = Object.extendsObject(OO2Model, {
    
    /*
    * Extended DB Model class which sits on top of a table.
    * and adds a layer on top which can include some query logic.     
    *
    * @author  Scott Ladyman
    * @version 2.0 (OO2), 06/2023
    *
    * OO2Object 
    *   |
    *   OO2Db 
    *       | 
    *       OO2Model
    *            |
    *            DB2Company
    *   
    */

    // REQUIRED
    o : {},
    
    initialize: function(mappings, elements) 
    {
    	OO2Model.prototype.initialize.call(this, {
            "table"     : "core_company",
            // base query for model
            "query"     : "",
            // return the GlideRecord Object or mapped fields
            "mappings"  : mappings || true,
            // return SN field elements
            "elements"  : elements || false,            
        });    	
    },


    /*
        Get Vendors off the table
    */
    vendors : function(query)
    {
        return this.query("vendor=true"+ (query ? "^"+ query : ""))
    },


    type: 'DB2Company'
});