var OO2Variables = Class.create();
OO2Variables.prototype = {
    
    db : {},

    // store the variables
    v : null,

    initialize: function(o) 
    {
        if (typeof o == "string") {
            if (o.length == 32) {
                var db = new GlideRecord("sc_req_item");
                if (db.get(o)) {
                    o = db;
                }
            }
        }

    	this.db = o;
   	},


    /*
	*	get the current varaible key object and reformats it.
	*	@k : string - key of variable field 
    */
    variable : function(k) 
    {
    	var db = this.db;
        if (db instanceof GlideRecord || db instanceof GlideRecordSecure) {
    		if (db.hasOwnProperty("variables") && db.variables.hasOwnProperty(k)) {
    			
    			var v = db.variables[k].getValue().trim();
		        var t = db.variables[k].getQuestion().getType();	
			 	//
			 	return {
			 		"key"     : k,
			        "value"   : (function(){
			            // 7:Checkbox  ||  1::Yes/No
			            if (t == 7 || t == 1) {
			                return (v === "true" || v === "Yes");
			            }
			            return v;
			        })(),
			        "display" : (function(){
			            if (t == 7) {
			                return (v === "true" ? "Yes" : "No" );
			            }
			            return db.variables[k].getDisplayValue().trim();
			        })(),			
			        "label"   : (db.variables[k].getQuestion().getLabel() || "").trim(),			
			        "table"   : (function(){
                        if (db.variables[k].hasOwnProperty("getListTable")) {
                            return db.variables[key].getQuestion().getListTable();
                        }                       
                    })(), 
			        "type"    : t 
			    }
    		}
    	}
    	return null;
    },


    /*
    *	Return the variables with questions attached
	*	@f : enum - filter by 'key' (name) 
    */
    get : function(f)
    {
    	var db = this.db;
        var a = [];
    	if ( db instanceof GlideRecord || db instanceof GlideRecordSecure) {
    		if (db.hasOwnProperty("variables")) {
                var aa = (typeof f === "string" ? f.replace(/,\s+/g, ",").trim().split(",") : []);
		    	for(var k in db.variables) {
                    if (!aa.length || aa.indexOf(k) !== -1) {
    		    		var o = this.variable(k, db);
    		    		
    		    		if (o) a.push(o);		    		
                    }
		    	}
		    }
	    }
    	return this.v = a;
    },


    /*
        Output object into list of values in array seperated by a colon. If anything custom is needed used 'Array.filter(function())' on the this.get() method. 
        @f : string||array of fields to be output
        @m : mappings... future?
    */
    list : function(f)
    {   
        var a = this.v || this.get(f);

        var aa = [];
        for (var i = 0; i < a.length; i++) {
            
            // filter out any empty values
            if (a[i].hasOwnProperty("value") && (a[i].value === "" || a[i].value === null)) continue;
            if (a[i].hasOwnProperty("label") && a[i].hasOwnProperty("display")) {
                aa.push(a[i].label +": "+ a[i].display);
            }
        }
        return aa;
    },


    type: 'OO2Variables'
};