var OO2Object = Class.create();
OO2Object.prototype = {
    
    /*
     * Base class for 'OO' library.  All classes based on the "OO" library will be extended from this class 
     *
     * @author  Scott Ladyman
     * @version 2.0 (OO2), 02/2023
     *
    */    
    //  REQUIRED
    o : {},
    
    
    initialize : function(o) 
    {
        var self = this;
        // reset
        this.o   = {};
        
        if (o) {
            if (o instanceof GlideRecord || o instanceof GlideRecordSecure) {
                this.o = o
            }else{
                // load passed object into object
                for (var k in o) {
                    // set the values of parameters
                    this.__set(k, o[k]);   
                    // remove any dot walked passed option otherwise it will wreck the object
                    if (k.indexOf(".") !== -1) delete this.o[k];
                } 
            }
        }
    },

    
    /*
        magic function that can do multiple things with an object        
    */
    oo : function(x, y, z)
    {   
             
        if (typeof x == "object") {
            //  rekey the array into an object based on a key
            if (x instanceof Array) {
                if (typeof y == "string") {
                    // rekey object based on key
                    var o = {};
                    for (var i = 0; i < x.length; i++) {
                        if (x[i].hasOwnProperty(y)) {
                            o[x[i][y]] = x[i]
                        }    
                    }
                    // return new rekeyed object
                    return o;
                // return unique only values
                }else if (typeof y == "boolean") {
                    // return unique only values
                    if (y) {
                        var a = [];
                        for (var i = 0; i < x.length; i++) {
                            if (a.indexOf(x[i]) === -1) a.push(x[i]);
                        }
                        return a;
                    }
                    return x;
                }else if (typeof y == "object") {
                    // create object from array, or merge the values.
                    for (var i = 0; i < x.length; i++) {
                        y[x[i]]= x[i];
                    }    
                    return y;                        
                }
            // merge object x into y     
            }else if (typeof y == "object") {
                //
                for(var k in x) {
                    y[k] = x[k];
                }
                // return new merged object
                return y;                
            }else if (typeof y == "string") {
                // set a value based on passed xpath
                if (typeof z != "undefined") {
                    return this.__set(y, z, x);
                }
                //  get xpath value of object
                return this.__get(y, z);    
            }
            // clone object
            return JSON.parse(JSON.stringify(x));

        // xpath down object    
        }else if (typeof x == "string") {
            // set a value based on passed xpath
            if (typeof y != "undefined") {
                return this.__set(x, y);
            }
            //  get xpath value of object
            return this.__get(x);    
        }
        
        //
        return this.o;     
    },


    /*
    *   Basic mapping of data into an object
    *   @m : object || comma sep string - use shorthand symbols for ease of use.
    *       @ - return string
    *       = - return boolean/number    
    *       $ - return .getDisplayValue() if GlideRecord passed. (default none is .getValue())                        
    *   @c : object || GlideRecord
    *   @o : object (return) 
    *   @f : boolean (true) - filter empty 
    */
    map : function(m, c, o, f)
    {
        // set up the object to be returned
        c = c || this.o;
        o = o || {};
        // allow blanks
        b = (typeof b == "undefined" ? true : b);

        // clean 'm' enum input incase it was a comma list
        if (typeof m == "string") m = this.parse(m, true);

        // convert the array to an object
        if (m instanceof Array) m = this.parse(m, {});

        //incase nothing was passed, purely for lookups
        if (!Object.keys(m).length){
            m.sys_id = "sys_id";
        }

        // map elements.
        for(var k in m) {
            if (typeof m[k] == "boolean" && m[k] === false)  continue;

            // get the value and clean
            var self = this;
            var v  = (function(v){
                //
                if (typeof v == "function") return v(c, k, self);
                if (typeof v == "object")   return v;
                if (typeof v == "string")   {
                    //  security issue?
                    if (v.indexOf("(function") === 0) {
                        // execute function
                        return self.func(v, c, self);
                        // return new Function("return "+ v)()(c, self);                        
                    }

                    // force string value return
                    if (v.indexOf("@") === 0) {
                        var s = v.substring(1, v.length);
                        if (s.indexOf("${") !== -1) {
                            return self.literals(s, c);   
                        }
                        return s;
                    }
                    if (v.indexOf("=") === 0) {
                        return v.substring(1, v.length);
                    }
                }
                //
                return self.__get((v === true ? k : v), c);

            })(m[k]);           
            //  
            if (typeof v != "undefined" && v != null && (!f || (f && v != ""))) {
                o[k] = (function(){
                    if (m[k].indexOf("@") === -1) {
                        if (v.toUpperCase() === "FALSE" || v.toUpperCase() === "TRUE") return (v.toUpperCase() == "TRUE");
                        // convert to number
                        if (!isNaN(v) && v !== "") return +v;
                    }
                    return v;
                })();
            }           
        }  

        return o;
    },


        
    /*
    *    Get JSON property, can DOT work object
    *    @k : string - eg. "some.object.property"
    *    @o : object
    */    
    __get : function(k, o)
    {       
        o = o || this.o;
        
        var v = k.replace(/\$/g, "").split('.').reduce(function(p, c) {   
            return (typeof p != "undefined" ? (typeof p[c] == "string" ? OO2Object.prototype.parse.call(this, p[c]) : p[c]) : p);            
        }, o);  

        if (o instanceof GlideRecord || o instanceof GlideRecordSecure) {            
            if (typeof v != "undefined") {                        
                // return display value
                if (k.indexOf("$") !== -1) return v.getDisplayValue();                        
                // 
                v = String(v);                        
            }            
        }
        return OO2Object.prototype.parse.call(this, v);
    },


    /*
    *    Set JSON property, can DOT work object
    *    @k : string - eg. "some.object.property"
    *    @v : ~ - value to be set
    *    @o : object - to set
    */
    __set : function(k, v, o)
    {
        o = o || this.o;
        
        var oo = o;
        var a = k.split(".");
        for (var i = 0; i < a.length - 1; i++) {
            if (!oo.hasOwnProperty(a[i])) {
                oo[a[i]] = {};
            }
            oo = oo[a[i]];
        }
        oo[a[i]] = v;

        return o;
    },


    /*
    *    Parse a value and evaluate it for an object, 
    *    @v : string - value to pass
    *    @f: boolean | delimiter - allows to pass enum string values better than split()
    *
    *    return json or original value
    */    
    parse : function(v, f)
    {
        if ((v.indexOf("[") === 0 && v[v.length -1] === "]") || ((v.indexOf("{") === 0 && v[v.length -1] === "}"))) {
            try{
                return JSON.parse(v.replace(/\"\{/g, "{").replace(/\}\"/g, "}"));
            }catch(e){}   
        }else if(typeof v === "string") {
            // delimiter requested ...better verson of split().
            if (f) {
                return v.split(new RegExp("\\s*"+ (f === true ? "," : f) +"\\s*", "g")).filter((function(v){
                    // remove any empty array values.
                    return (v.length);
                }));
            }
        }else if (v instanceof Array) {
            // delimiter requested ...better verson of split().
            if (f) {
                return this.oo(v, {});
            }
        }
        //
        return v;
    },


    /*
        @s : string
        @o : object - object to pass for mapping
        @m : object - function mappings for complex literals
    */
    literals : function(s, o, m)
    {
        o = o || this.o;       
        
        // set any mapping
        if (m) var oo = this.map(m, o);     

        // process the template
        var self = this;
        return s.replace(/\$\{(.*?)\}/g, function(m, p) {
            // extract and default message
            var a = p.split("||");            
            // was there a mapping callback?
            if (typeof oo != "undefined" && oo.hasOwnProperty(a[0])) {
                var v = oo[a[0]];
            }else{
                // get the value
                var v = self.__get(a[0].trim(), o);    
            }

            // any default value to be set if empty?
            if (a.length > 1) {
                if (typeof v == "undefined" || (typeof v != "undefined" && v == "")){
                    return a[1].trim(); 
                }
            }
            return (typeof v != "undefined" && v != "" ? v : m);
        });
    },
    

    /*
        Executes string function
        @f : string
        @x, y, z : parameters of function
    */
    func : function(f, x, y, z)
    {
        return new Function("return "+ f)()(x, y, z);
    },


    /*
        convert object into readable object
        @o : object
    */
    tidy : function(o) 
    {
        return (typeof o == "object" ? JSON.stringify(o, null, "\t") : o);        
    },


    /*
        Pass anything to the logs
    */
    log : function(x)
    {
        gs.log(this.tidy(x));
    },


    type: 'OO2Object'
};