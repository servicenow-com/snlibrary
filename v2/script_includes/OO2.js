/*
	OO Class library - CONSTANTS
*/
var OO2 	= Class.create();
OO2.CNAME 	= gs.getProperty("oo.cname") || "105861";
OO2.ENV   	= (function(){
	var n = gs.getProperty("instance_name").replace(OO2.CNAME, "").toUpperCase();	
	return (n == "" && (gs.getProperty("glide.installation.production").toUpperCase() === "TRUE") ? "PROD" : n);
}());
//
OO2.DEBUG    = gs.getProperty("oo.debug") || (OO2.ENV == "DEV" && gs.hasRole("admin"))	
//	global functions
OO2.property = (function(v, d) {
	// ensure String proptype is avaliable.
	v = gs.getProperty(v);
	if (v != null) {
		if (v.isNum(v)) return parseInt(v);
		if (v.toUpperCase() === "TRUE" || v.toUpperCase() === "FALSE") return (v.toUpperCase() === "TRUE");

		if (v.indexOf("(function") === 0) {
			// execute function
			return new OO2Object().func(v);
		}

		// see if we can force the value into an object
		v = new OO2Object().parse(v, f);
	}
	return (v != "" && v != null ? v : null);
});
//
OO2.log = (function(v, s) {
	if (OO2.DEBUG) {
		gs.log((typeof v == "object" ? JSON.stringify(v, null, "\t") : v), s);
	}
});