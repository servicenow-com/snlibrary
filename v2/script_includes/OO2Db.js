var OO2Db = Class.create();
OO2Db.prototype = Object.extendsObject(OO2Object, {
    
    /*
     * DB class for the 'OO' library. 
     *
     * @author  Scott Ladyman
     * @version 2.0 (OO2), 02/2023
     * 
    */
    
    // REQUIRED
    o : {},
    
  
    /*
        @o : string || GlideRecord - pass table name or glide
        @s : boolean - Use GlideRecordSecure
    */
    initialize: function(o, secure) 
    {
        var self = this;
        OO2Object.prototype.initialize.call(this, this.oo((function(o){
            if (typeof o == "string") {
                return {
                    // set the table.
                    "table" : o
                }
            }
            return o || {};
        })(o), {
            "db"        : null,
            "table"     : "task",
            "query"     : "active=true",
            "mappings"  : "sys_id,number,name,short_description",
            "data"      : null,            
        }));               

        // init. the DB object
        this._db(this.o.table, secure);
    },


    /*  
    *   return glide object
    *   @t : string - table name
    *   @s : boolean - secure? 
    */
    _db : function(t, s)
    {
        var db = (s === true ? new GlideRecordSecure(t) : new GlideRecord(t));
        // this.o.db = db;
        this.oo("db", db);
        this.oo("table", t);

        return this;
    },


    /*  
    *   Sets the mapping fields
    *    return this
    *   @m : object - mappings
    */
    fields : function(m)
    {
        this.oo("mappings", (m || this.o.mappings));

        return this;
    },


    /*
    *   Get JSON property, can DOT work object
    *   @q : string - "query" - pass null for all
    *   @m : object - mappings  > "*" can get all fields || boolean to return GlideRecord
    *   @e : boolen - get field elements
    *   @p : boolean  - [true, default] parse and convert into a multi-dimensional object or [false] keep it a flat object
    *
        // MAPPING FIELD EEXAMPLES
        var m = null;
        var m = true; // GlideRecord can be set to false
        var m = "*"; // all fields
        var m = "sys_id,user_name";
        var m = ['sys_id', "email"];
        var m = {
            "sys_id"        : "sys_id",     // get field
            "depart"        : "$department",    // get display value
            "department"    : "department.description",     // dot walk
            "boolean"       : "=true",      // set boolean
            "number"        : "=1234",      // set number
            "short"         : "@Email address ${email}", // set string, and allows for literals from the record
            "description"   : (function(gr){ // callback on field
                // transform from glide
                return [gr.name, gr.user_name, gr.email].join(", ")
            }),
            // advance object build
            "manager.email"         : "manager.email",      // dot walk and build object
            "manager.user_name"     : "manager.user_name",  // dot walk
            "manager.name.full"     : "$manager",           // display value
            "manager.name.first"    : "manager.first_name", // dot walk
            "manager.name.last"     : "manager.last_name",  // dot walk
        }     
    */
    query : function(q, m, e, p)
    {                      
        var self = this;
        this.oo("query", (function(){
            if (q === "" || q) {
                return q;
            }
            return self.o.query.replace(/\^\^/, '^').replace(/^\^/, '');
        })());
        this.oo("mappings", (m || this.o.mappings));



        var db = this.oo("db");
        if (db instanceof GlideRecord || db instanceof GlideRecordSecure) {
            // DB current object
            db.addEncodedQuery(this.o.query);
            db.query();

            m = this.o.mappings;
            
            if (typeof m != "boolean") {          
                var a = [];
                var i = 1;
                while (db.next()) {                    
                    
                    // get all fields?
                    if (m == "*" && i === 1) {
                        m = ["sys_id"];
                        var aa = db.getFields();
                        for (var i = 0; i < aa.size(); i++) {
                            m.push(String(aa.get(i).getName()));
                        }

                    }        

                    if (e) {
                        // too do. retrieve table fields
                        a.push(this.elements(m));
                    }else{
                        var self = this;
                        a.push((function(){
                            var o = self.map(m, db);
                            if (typeof o == "object" && (typeof p == "undefined" || p !== false)) { 
                                return new OO2Object(o).oo();
                            }
                            return o;
                        })()); 
                    }   
                    i++;
                }
                return this.o.data = a;
            }
            return db;
        }
        return null;
    },
 

    /*
    *   Get single record
    *   @q : string - "query"
    *   @m : object - mappings
    *   @e : boolen - get field elements
    *   @p : boolean  - [true, default] convert into a multi-dimensional object or [false] keep it a flat object
    */
    single : function(q, m, e, p)
    {        
        var db = this.limit(1).query(q, m, e, p);
        //  if what was returned was a GR object, as no mappings were set.
        //  Code wants only the GR object instead.
        if (db instanceof GlideRecord || db instanceof GlideRecordSecure) {            
            // 
            if (db.next()) {
                //
                return db;    
            }
            return null;            
        }
        
        if (db.length) {
            return this.data = db[0];
        }
        return null;
    },


    /*
    *    Short cut to get record by id/number
    *    @q : string - "query"
    */
    get : function(id, m, e, f)
    {
        return this.single((String(id).length == 32 ? "sys_id" : "number") +"="+ id, m, e, f)
    },


    /*
    *   Aggregate count
    *
    *   @q : query    
    */
    count : function(q)
    {
        this.oo("query", q || this.o.query);
        
        var db = new GlideAggregate(this.oo("table"));    
        db.addAggregate('COUNT', 'count');
        db.addEncodedQuery(this.oo("query"));
        db.query();
        //
        if (db.next()) {
            return db.getAggregate('COUNT', 'count');
        }
        return 0;
    },


    /*
    *   Limit record, using offset for pagination
    *
    *   @limit : string 
    *   @offset : string - for pagination
    */
    limit : function(limit, offset)
    {
        var db = this.oo("db");
        if (db instanceof GlideRecord || db instanceof GlideRecordSecure) {
            if (typeof limit != "undefined" && limit != null) {
                if (typeof limit == "string")   limit = parseInt(limit);
                if (typeof offset != "undefined"){
                    if (typeof offset == "string")  offset = parseInt(offset);              
                    offset--;
                    db.chooseWindow(limit * (offset < 0 ? 0 : offset), (limit * (offset < 0 ? 0 : offset)) + limit , true);
                }else{
                    db.setLimit(limit);
                }
            }
        }
        return this;
    },


    /*
    *    Update records
    *    @o : object - data to be updated
    *    @q : string - where query of records to be updated
    */
    update : function(o, q)
    {
        this.oo("query", q || "");

        var db = this.oo("db");
        if (db instanceof GlideRecord || db instanceof GlideRecordSecure) {
            db.addEncodedQuery(this.oo("query"));
            db.query();
            
            var a = [];
            while (db.next()) {
                var oo = {};
                for (var k in o) {
                    oo[k] = this._set(k, o[k]);                                     
                }
                //
                if (db.update()) {
                    oo.sys_id = db.getUniqueValue();
                    // add updated record
                    a.push(oo);
                }
            }
            return a;
        }
        return false;
    },


    /*
    *    Update records
    *    @o : array||object - data to be inserted        
    */
    insert : function(o)
    {
        var db = this.oo("db");
        if (db instanceof GlideRecord || db instanceof GlideRecordSecure) {
            if (!(o instanceof Array)) {
                //  setup the data into the 'bulk' loading mech.
                o = [o];
                // bulk data not requested
                b = false;          
            }

            var a = [];
            for(var i = 0; i < o.length; i++){              
                var oo = {};
                for (var k in o[i]) {
                    oo[k] = this._set(k, o[i][k]);                                      
                }                               

                if (db.insert()) {
                    oo.sys_id = db.getUniqueValue();
                    // add updated record
                    a.push(oo);
                }
            }
            // was only one record was to be inserted?
            return (typeof b != "undefined" ? a[0] : a);
        }

        return false;
    },


    /*
    *    get the table record fields
    *    @m : mixed - mappings 
    *    @f : boolean - filter empty results?
    */
    elements : function(m, f)
    {
        var db = this.oo("db");

        // clean 'm' enum input incase it was a comma list
        if (typeof m == "string") m = m.replace(/\s/g, "").split(",");

        // convert the array to an object
        if (m instanceof Array) {
            var oo = {};
            for (var i = 0; i < m.length; i++) {
                oo[m[i]]= m[i];
            }    
            m = oo;
        }

        //incase nothing was passed, purely for lookups
        if (!Object.keys(m).length){
            m.sys_id = "sys_id";
        }
        
        // get the dictionary label values incase they were over written.
        var d = (function(){            
            var a = Object.keys(m).split(",");
            var gr = new GlideRecord("sys_documentation");
            gr.addEncodedQuery("name="+ db.sys_class_name.getValue() +"^elementIN"+ a.join(","));
            gr.setLimit(a.length);
            gr.query();
            
            var oo = {};
            while (gr.next()) {
                oo[gr.element.getValue()] = gr.label.getValue();
            }
            return oo;
        })();

        var o = {};
        for(var k in m) {
            var e = db.getElement(m[k].replace("$", ""));

            // this.log(m[k] +" - "+ e)
            if (e !== null) {
                // get the field type
                var t = String(e.getED().getInternalType().toLowerCase());
                var v = e.getValue();
                // filter unwanted empty fields
                if (!f || (f && v != "" && v != null)) {
                    o[k] = {
                        "label"   : (function(){
                            // if there is a dictionary override
                            if (d.hasOwnProperty(k)) return d[k];

                            return e.getED().getLabel();
                        })(),
                        "value"   : (function(){
                            switch (t) {
                                case "boolean" :
                                    return v == "1" ? true : false; 
                                break;
                                case "float":
                                case "integer":
                                    return +(v);
                                break;
                                default :
                                    return v;
                                break;
                            }
                        })(),
                        "display" : (function(){
                            switch (t) {
                                case "boolean" :
                                    return v == "1" ? "Yes" : "No"; 
                                break;                          
                                default :
                                    return e.getDisplayValue();
                                break;
                            }                       
                        })(),
                        "type"  : t,
                        "table" : (function(){
                            if (t == "reference") {
                                return db.getElement(m[k].replace("$", "") +".sys_id").getTableName();
                            }
                        })(),
                        "field" : k
                    };                    
                }
            }
        }
        return o;
    },


    /*
    *    sets value against Glide Object
    */
    _set : function(k, v)
    {       
        var db = this.db;
        //  
        if (typeof v == "function") v = v(db);

        if (v != null && typeof v != "undefined") {
            if (v instanceof Object) v = JSON.stringify(v); 

            if (k.indexOf("$") === 0) {
                // add to jourunal table
                db.setDisplayValue(k.replace("$", ""), v);
            }else{
                db.setValue(k, v);
            }

            return v;           
        }       
        return null;
    },


    /*
    *    return object for a Portal watcher
    */
    watcher : function()
    {
        return {
            "table" : this.oo("table"),
            "query" : this.oo("query")
        };
    },


    /*
    *    rekey the returned results into an object with a field from within
    *   the returned data as the key   
    *
    *    @f : string - field from within data to be the new object key 
    */
    rekey : function(f)
    {
        return this.oo(this.o.data, f);
    },



    type: 'OO2Db'
});