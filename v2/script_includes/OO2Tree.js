var OO2Tree = Class.create();
OO2Tree.prototype = Object.extendsObject(OO2Object, {

    /*
     * Tree relationship class of "OO" library.
     * Ability to retrieve children or parents of related records.
     *
     * @author  Scott Ladyman
     * @version 2.0 (OO2), 02/2023
     * 
    */
    
    // REQUIRED
    o : {},

    initialize : function(o) 
    {
        OO2Object.prototype.initialize.call(this, this.oo(o || {}, {
            "child"     : "sys_id",
            "parent"    : "parent",
            "registry"  : [],
            "table"     : "cmdb_rel_ci",
            "query"     : "",            
            //  enum | array | object 
            //  empty value will be handled by OO2Object mapping method
            "mappings"  : []
        }));
    },


    /*
    *   Retrieve the categories of the passed parent_id
    *   @id: String
    *   @return     : Array
    */
    parents : function(id)
    {
        var self = this;
        var db = new GlideRecord(this.o.table);      
        db.addEncodedQuery(this.o.child +"="+ id + (function(){
            if (self.o.query != "") {
                return "^"+ self.o.query.replace(/^\^/, '');
            }
            return "";
        })());
        db.query();
        
        var a = [];
        while (db.next()) {

            a.push(this.map(this.o.mappings, db));                       
            //
            a.unshift.apply(a, OO2Tree.prototype.parents.call(this, db.getValue(this.o.parent)));
        }
        return a;
    },

    /**
    *   Retrieve the categories of the passed parent_id
    *   @parent_id  : String
    *   @maxdepth   : Int - limit the search. 0 would equal the cats of the passed parent only, no cascading
    *   @depth      : Int - Current depth    
    *   @return     : Array
    */
    children : function(parent_id, maxdepth, _depth)
    {   
        var self = this;
        if (typeof(_depth) == "undefined") {
            //
            this.o.mappings[this.o.parent] = this.o.parent;
            //
            _depth = 1;
        }
        
        var db = new GlideRecord(this.o.table);      
        db.addEncodedQuery(this.o.parent +"="+ (typeof parent_id == "boolean" ? "" : parent_id) + (function(){
            if (self.o.query != "") {
                return "^"+ self.o.query.replace(/^\^/, '');
            }
            return "";
        })());
        db.query();

        var a = [];
        while (db.next()) {
            // ensure that we don't 'loop' due to a parent existing as a child
            if (this.o.registry.indexOf(db.getValue(this.o.mappings.sys_id || "sys_id")) !== -1 ) {
                continue;
            }
            //  setup data object
            var o = this.map(this.o.mappings, db, {"sys_id" : db.getValue("sys_id")});                        
            o._depth = _depth;
            //
            a.push(o);
            //
            this.o.registry.push(this.o.mappings.sys_id);
            
            if (typeof maxdepth == "undefined" || _depth < maxdepth) {
                a.push.apply(a, OO2Tree.prototype.children.call(this, db.getValue(this.o.mappings.sys_id || "sys_id"), maxdepth, _depth + 1));
            }           
        }
        return a;
    },

    
    type: 'OO2Tree'
});