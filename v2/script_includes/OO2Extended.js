var OO2Extended = Class.create();
OO2Extended.prototype = Object.extendsObject(OO2Object, {
    
    o : {},

    initialize: function(o) 
    {
    	OO2Object.prototype.initialize.call(this, this.oo(o || {}, {
            "two" : "2",
            "a.b" : "adasdad"            
        }));        
    },

    type: 'OO2Extended'
});