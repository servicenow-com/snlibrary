var OO2Model = Class.create();
OO2Model.prototype = Object.extendsObject(OO2Db, {
    
    /*
    * A Model base class that is uased as a base class to extend a table lookups
    * Do not use this directly, extend only. i.e new DB2user()....    
    *   
    *
    * @author  Scott Ladyman
    * @version 2.0 (OO2), 06/2023
    *
    * OO2Object 
    *   |
    *   OO2Db 
    *       | 
    *       OO2Model
    *            |
    *           ...extended class
    */

    // REQUIRED
    o : {},

    // store the query as the DB class will overwrite it.
    q : "",

    initialize: function(o) 
    {
    	OO2Db.prototype.initialize.call(this, this.oo((function(o){
            if (typeof o == "string") {
                return {"table" : o};
            }
            return o || {};
        })(o), {
            // by default return GlideRecord Object
            "mappings"  : true            
        }));   

        this.q = this.o.query;       
    },

    
    /*
	
    */
    query : function(query, mappings, elements, parse)
    {
		// be sure to reset the query
        this.o.query = this.q;
        
        return OO2Db.prototype.query.call(this, 
			this.o.query + (query ? "^"+ query : ""),
			mappings || this.o.mappings,
            elements || this.o.elements,
            parse
		);        
    },



    /*
	   
    */
    single : function(query, mappings, elements, parse)
    {
		// Built this way to stop class inheritance which will call the above 'query' method
        // instead of the one within the extended class, as it will duplicate the query.        
		return new OO2Db(this.o.table).single(
			this.o.query + (query ? "^"+ query : ""), 
			mappings || this.o.mappings,
            elements || this.o.elements,
            parse
		);
    },


    /*
    
    */
    get : function(id, mappings, elements, parse)
    {
        return this.single(
            this.o.query +"^"+ (String(id).length == 32 ? "sys_id" : "number") +"="+ id,
            mappings || this.o.mappings,
            elements || this.o.elements,
            parse
        );
    },


    /*

    */
    find : function(array, field)
    {

    },

    type: 'OO2Model'
});