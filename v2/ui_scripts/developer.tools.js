var oo = {
	"o" : {
		// localStorageKey
		"storage" : "oo.developer.tools"
	},
	"gform" : function(){
		return $('sp-variable-layout').scope().getGlideForm();
	},
	"storage" : {
		"get" : function(o) {
			if (window) {
				var v = window.localStorage.getItem(oo.o.storage);
				if (v != "" && v != null) {
					return JSON.parse(v);
				}
			}
			return null;
		},
		"set" : function(o) {
			if (window) {
				window.localStorage.setItem(oo.o.storage, (typeof o == "object" ? JSON.stringify(o) : o));				

				return o;
			}
			return null;
		}
	},
	"form"  : {		
		"item" : function(){
			var scope = $('sp-variable-layout').scope();
			if (scope) {
				return scope.formModel;
			}
			return null;
		},
		"fields" : function(){
			var form = this.item();
			if (form.hasOwnProperty("_fields")) {
				return form._fields;
			}
			return null;			
		},
		"get" : function(field) {
			var gform = oo.gform();			
			if (gform) {
				if (typeof field != "undefined") {
					return gform.getValue(field);
				}

				var fields = this.fields();
				var o = {};
				for (var f in fields) {
					if (fields[f].hasOwnProperty('isVisible') && fields[f].isVisible()){
						var v = gform.getValue(f);
						if (v != "" && v != null) {
							o[f] = gform.getValue(f);		
						}						
					}					
				}
				return o;
			}
			return null;
		},
		"set" : function(o, v) {
			var gform = oo.gform();
			if (gform) {
				if (typeof o != "undefined") {
					if (typeof o == "object") {
						for (var f in o) {
							setTimeout(gform.setValue(f, o[f]), 100);		
						}						
					}else{
						gform.setValue(o, v);	
					}
				}
			}
			return (typeof o == "object" ? o : v);			
		},
		/*
			Save the current values in the form to localStorage 
		*/
		"save" : function() {			
			var o = oo.storage.get() || {};
			var f = this.item();
			
			if (!o.hasOwnProperty("form")) {
				o.form = {};	
			}				
			//
			o.form["@"+ f.sys_id] = this.get() || {};			
			//	set the form
			oo.storage.set(o);			
		},

		// "reset" : function()
		// {
		// 	var gform = oo.gform();			
		// 	if (gform) {
		// 		var fields = this.fields();
		// 		for (var f in fields) {
		// 			if (fields[f].hasOwnProperty('isVisible') && fields[f].isVisible()){
		// 				gform.setValue(f, "");						
		// 			}					
		// 		}				
		// 	}			
		// }
		// check for any existing 
		"onload" : function()
		{						
       		var f = oo.form.item();
			if (f) {
				// prepopulate the form.
				var o = oo.storage.get();					
				if (typeof f == "object" && typeof o == "object") {
					if (o.hasOwnProperty("form") && f.hasOwnProperty("sys_id") && o.form.hasOwnProperty("@"+ f.sys_id)) {
						oo.form.set(o.form["@"+ f.sys_id]);
						x = true;
					}
				}
			}			       
		}
	}	
}
// oo.form.onload();