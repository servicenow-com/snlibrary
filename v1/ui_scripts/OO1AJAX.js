function OO1AJAX(properties){    
    /*
    How to use in client scripts...
    new OO1AJAX({
        "table"     : "cmn_location",
        "query"     : "active=true",
        "limit"     : "1",  
        "data"      : "field,names"
        ||
        "script"     : "ScriptInclude",
        "method"     : "Method",    
        "data"       : {}
    }).then(function(response){
        console.log(response)
    });    
    */
    var o = {
        "mapping" : {
            "name"  : "method",
            "table" : "table",
            "query" : "query",
            "limit" : "limit",
            "data"  : "data"
        },
        //  Script Include to call..
        "script"   : "OO1AJAX",
        "method"   : "query",
        "table"    : null,
        "query"    : null,
        "limit"    : null,
        "data"     : "sys_id",
        "callback" : null
    };

    if (typeof properties != "undefined"){
        if (typeof properties == "object") {
            for (var key in properties) o[key] = properties[key];                
        }  
    }
    
    var ajax = new GlideAjax(o.script);
    for (var k in o.mapping) {
        var v = o[o.mapping[k]];
        ajax.addParam("sysparm_"+ k, (typeof v == "object" ? JSON.stringify(v) : v));
    }

    return {
        "then" : function(callback){
            //
            ajax.getXML(function(response){
                // run the callback on response of the request
                callback(response.responseXML.documentElement.getAttribute("answer"));
            })
        }
    }
};