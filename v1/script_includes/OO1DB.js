var OO1Db = Class.create();
OO1Db.prototype = Object.extendsObject(OO1Object, {
	
    /*
		@o : string || GlideRecord - pass table name or glide
		@s : boolean - Use GlideRecordSecure
	*/
    initialize: function(o, s) 
    {
        var self = this;
        OO1Object.prototype.initialize.call(this, this.oo((function(){
            
            if (o instanceof GlideRecord || o instanceof GlideRecordSecure) {
                return {
                    "db"    : o,
                    "table" : o.getTableName()
                }                
            }else if (typeof o == "string") {
                //
                self.Db(o, s);
                //
                return {
                    "db"    : self.o.db,
                    "table" : o
                }
            }
            return o || {};
        })(), {
            "db"        : null,
            "table"     : "task",
            "query"     : "",
            "mappings"  : "number, short_description",
        }));
    },


    /*	
    *	return glide object
    *	@t : string - table name
	* 	@s : boolean - secure? 
    */
    Db : function(t, s)
    {
    	var db = (s === true ? new GlideRecordSecure(t) : new GlideRecord(t));
        // this.o.db = db;
        this.oo("db", db);
        this.oo("table", t);

    	return this;
    },


    /*
    *    Get JSON property, can DOT work object
    *    @q : string - "query" - pass null for all
    *    @m : object - mappings
    	 @e : boolen - get field elements
    	 @f : function || boolean  - callback filter or remove blanks
    */
    query : function(q, m, e, f)
    {    	
    	this.oo("query",    q || this.o.query);
        this.oo("mappings", m || this.o.mappings);

        var db = this.oo("db");
    	if (db instanceof GlideRecord || db instanceof GlideRecordSecure) {
	    	// DB current object
	    	db.addEncodedQuery(this.oo("query"));
		    db.query();

		    if (m != false) {		   
		    	var a = [];
		    	while (db.next()) {
		    		//	condition filter on every record
	    			if (typeof f == "function") {
	    				if (!f(db)) {
	    					continue;
	    				}
	    			}

                    m = this.oo("mappings");                     
	    			// get all fields?
    				if (m == "*") {
    					m = [];
    					var aa = db.getFields();
                		for (var i = 0; i < aa.size(); i++) {
                			m.push(String(aa.get(i).getName()));
                		}
    				}
	    			if (e) {
	    				// too do. retrieve table fields
	    				a.push(this.elements(m, (typeof f != "function" ? f : null)));
	    			}else{
	    				// map record
	    				a.push(this.map(m, db, null, (typeof f != "function" ? f : null)));	
	    			}		    		
		    	}
		    	return a;
		    }
		    return db;
		}
		return null;
    },


    /*
    *    Get JSON property, can DOT work object
    *    @q : string - "query"
    *    @m : object - mappings
    	 @e : boolen - get field elements
    	 @f : function  - callback filter
    */
    single : function(q, m, e, f)
    {
		var db = this.limit(1).query(q, m, e, f);
		//	if what was returned was a GR object, as no mappings were set.
		//	Code wants only the GR object instead.
		if (db instanceof GlideRecord || db instanceof GlideRecordSecure) {
			// 
			if (db.next()) {
                //
                return db;    
            }
            return null;			
		}
		
		if (db.length) {
			return db[0];
		}
		return null;
    },


    /*
	*	Limit record, using offset for pagination
	*
	* 	@limit : string 
	*	@offset : string - for pagination
	*/
    limit : function(limit, offset)
    {
    	var db = this.oo("db");
    	if (db instanceof GlideRecord || db instanceof GlideRecordSecure) {
	    	if (typeof limit != "undefined" && limit != null) {
				if (typeof limit == "string") 	limit = parseInt(limit);
				if (typeof offset != "undefined"){
					if (typeof offset == "string") 	offset = parseInt(offset);				
					offset--;
					db.chooseWindow(limit * (offset < 0 ? 0 : offset), (limit * (offset < 0 ? 0 : offset)) + limit , true);
				}else{
					db.setLimit(limit);
				}
			}
		}
    	return this;
    },


    /*
		Update records
		@o : object - data to be updated
		@q : string - where query of records to be updated
    */
    update : function(o, q)
    {
    	this.oo("query", q || "");

    	var db = this.oo("db");
    	if (db instanceof GlideRecord || db instanceof GlideRecordSecure) {
    		db.addEncodedQuery(this.oo("query"));
    		db.query();
    		
    		var a = [];
    		while (db.next()) {
    			var oo = {};
    			for (var k in o) {
    				oo[k] = this._set(k, o[k]);    				    				
    			}
    			//
    			if (db.update()) {
    				oo.sys_id = db.getUniqueValue();
    				// add updated record
    				a.push(oo);
    			}
    		}
    		return a;
    	}
    	return false;
    },


    /*
		Update records
		@o : array||object - data to be inserted		
    */
    insert : function(o)
    {
    	var db = this.oo("db");
    	if (db instanceof GlideRecord || db instanceof GlideRecordSecure) {
    		if (!(o instanceof Array)) {
				//	setup the data into the 'bulk' loading mech.
				o = [o];
				// bulk data not requested
				b = false;			
			}

	    	var a = [];
			for(var i = 0; i < o.length; i++){				
				var oo = {};
				for (var k in o[i]) {
    				oo[k] = this._set(k, o[i][k]);    				    				
    			}    							

    			if (db.insert()) {
    				oo.sys_id = db.getUniqueValue();
    				// add updated record
    				a.push(oo);
    			}
			}
			// was only one record was to be inserted?
			return (typeof b != "undefined" ? a[0] : a);
    	}

    	return false;
    },


    /*
		get the table record fields
		@m : mixed - mappings 
		@f : boolean - filter empty results?
    */
    elements : function(m, f)
    {
    	var db = this.oo("db");

    	// clean 'm' enum input incase it was a comma list
    	if (typeof m == "string") m = m.replace(/\s/g, "").split(",");

    	// convert the array to an object
    	if (m instanceof Array) {
    		var oo = {};
            for (var i = 0; i < m.length; i++) {
                oo[m[i]]= m[i];
            }    
            m = oo;
    	}
        m._sid = "sys_id";

        // get the dictionary label values incase they were over written.
    	var d = (function(){    		
    		var a = Object.keys(m).split(",");
    		var gr = new GlideRecord("sys_documentation");
    		gr.addEncodedQuery("name="+ db.sys_class_name.getValue() +"^elementIN"+ a.join(","));
    		gr.setLimit(a.length);
    		gr.query();
    		
    		var oo = {};
    		while (gr.next()) {
    			oo[gr.element.getValue()] = gr.label.getValue();
	   		}
    		return oo;
    	})();


    	var o = {};
    	for(var k in m) {
    		var e = db.getElement(m[k].replace("$", ""));

            if (e !== null) {
    			// get the field type
    			var t = String(e.getED().getInternalType().toLowerCase());
    			var v = e.getValue();
    			// filter unwanted empty fields
    			if (!f || (f && v != "" && v != null)) {
    				o[k] = {
    					"label"	  : (function(){
    						// if there is a dictionary override
    						if (d.hasOwnProperty(k)) return d[k];

    						return e.getED().getLabel();
    					})(),
    					"value"   : (function(){
    						switch (t) {
    							case "boolean" :
    								return v == "1" ? true : false; 
    							break;
    							case "float":
    							case "integer":
    								return +(v);
    							break;
    							default :
    								return v;
    							break;
    						}
    					})(),
    					"display" : (function(){
    						switch (t) {
    							case "boolean" :
    								return v == "1" ? "Yes" : "No"; 
    							break;    						
    							default :
    								return e.getDisplayValue();
    							break;
    						}    					
    					})(),
    					"type"  : t,
    					"table" : (function(){
    						if (t == "reference") {
    							return db.getElement(k.replace("$", "") +".sys_id").getTableName();
    						}
    					})()
    				};    		
    			}
            }
    	}
    	return o;
    },


    /*
		sets value against Glide Object
    */
    _set : function(k, v)
    {    	
    	var db = this.db;
    	//	
    	if (typeof v == "function") v = v(db);

    	if (v != null && typeof v != "undefined") {
    		if (v instanceof Object) v = JSON.stringify(v); 

    		if (k.indexOf("$") === 0) {
    			// add to jourunal table
				db.setDisplayValue(k.replace("$", ""), v);
    		}else{
    			db.setValue(k, v);
    		}

    		return v;    		
    	}    	
    	return null;
    },


    /*
        return object for a Portal watcher
    */
    watcher : function()
    {
        return {
            "table" : this.oo("table"),
            "query" : this.oo("query")
        };
    },



    type: 'OO1Db'
});