var OO1RESTExtended = Class.create();
OO1RESTExtended.prototype = Object.extendsObject(OO1REST, {
   	
   	o : {},
   	
    initialize: function(properties) 
    {
       OO1REST.prototype.initialize.call(this, (function(o){    
            //  pass any defaults options.
            if (typeof properties == "object") {
                for (var key in properties) o[key] = properties[key];                
            }  
            return o;       
        })({
       		"rules" : {
       			"sys_id" : {
			        "required" : true,
			        // "type"     : "email", 
			        // "regexp"   : "com\.au"                      
			    },
			    "state" : {
			    	"required" 	: true,
			        "format" 	: "lower",
			        "enum"   	: ["scheduled", "inprogress", "success", "failed"]
			    }			    
       		}
        })); 
    },


    // //	what to do on success
    // success : function(data)
    // {
		//	something went wrong...
		// OO1REST.prototype.failure.call(this, null, "Permission denied.", 403)
    // },


    // failure : function(data, errors)
    // {
    		// OO1REST.prototype.failure.call(this)           
    // },
   
    type : 'OO1RESTExtended'
});