var OO1AJAX = Class.create();
OO1AJAX.prototype = Object.extendsObject(AbstractAjaxProcessor, {
	
	_initialize : function()
	{	
		// NOTE: OO1DB is called in this->query() method
		var self = this;		
		this.OOBase = new OO1Base((function(){
			var o = {};
			var a = ["script","method","table","query","limit","data"];		
			for (var i = 0; i < a.length; i++) {
				var v = String(self.getParameter('sysparm_'+ a[i]));
				if (v != "null") {					
					o[a[i]] = (v[0] == "[" || v[0] == "{" ? JSON.parse(v) : v) 
				}
			}			
			return o;
		})())
	},

	//	return the reponse as a string.
	response : function(data)
	{
		if (typeof data == "object") {
            data = JSON.stringify(data);          
        }
        return data;
	},

	
	//	basic query.
	query : function()
	{
		//	call the init. method.
		OO1AJAX.prototype._initialize.call(this);	

		var db = new OO1DB(this.OOBase.oo("tbl"), false).data(this.OOBase.oo("data"));
		
		if (this.OOBase.oo("lmt") == '1') {
			return JSON.stringify(
				db.single(this.OOBase.oo("qry"))
			);			
		}

		return JSON.stringify(
			db.limit(this.OOBase.oo("lmt")).query(this.Base.oo("qry"))
		);		
	}
});