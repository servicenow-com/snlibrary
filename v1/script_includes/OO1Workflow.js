var OO1Workflow = Class.create();
OO1Workflow.prototype = Object.extendsObject(OO1Base, {

    /*    
    Note: Class designed to be called from within Workflows and activities 

    Caution: Defined variables in workflow activities are brought into class scope and 
    will cause issues within these method as defined variables in activities are global in scope.
    - Variable 'wf' is an example. 

    Note: 'current' and 'worflow' variable are global, and brought into class scope.

    */
    
    o : {},
    
    initialize: function(properties)
    {
        OO1Base.prototype.initialize.call(this, (function(o){            
            //  pass any defaults options.
            if (typeof properties == "object") {
                for (var key in properties) o[key] = properties[key];                
            }  
            return o;       
        })({
           
        }));     
    },


    /*
    *    Set a workflow scratchpad variable. 
    *    - Due to limitations in SN with passing objects in the workflow srcatchpad, this handles that limitation
    *   @key   | string 
    *   @value | anything
    */
    set : function(key, value)
    {
        var sp = workflow.scratchpad;

        for(var k in sp) {
            //  convert to object otherwise it won't work
            if (sp[k].indexOf("{") === 0) {
                sp[k] = JSON.parse(sp[k].replace(/'/gi, '\"'));
            }
        }
        //  
        sp = this.JSON().set(key, value, sp);

        //  convert back into a scratchpad SN object so it can be passed to other activities
        for (var k in sp) {
            sp[k] = (typeof sp[k] == "object" && !(sp[k] instanceof Array) ? JSON.stringify(sp[k]) : sp[k]);            
        }

        return value;        
    },


    /*
    *    Get a workflow scratchpad variable.
    *   @key   | string 
    *
    */
    get : function(key)
    {
        return this.JSON(workflow.scratchpad).get(key);       
    },



    /*
    *   Execute workflow
    *   @name | string - Name of workflow, cannot use ID as it changes each time it's published.
    *   @current   | object - GlideRecord Object
    *   @variables | object
    */
    execute : function(name, object, variables)
    {
        var _wf = new Workflow();
        return _wf.startFlow(_wf.getWorkflowFromName(name), object, object.operation(), variables);
    },


    type: 'OO1Workflow'
});