var OO1JSON = Class.create();
OO1JSON.prototype = {

    o : {},

    initialize : function(json) 
    {
        this.init(json);
    }, 

    /*
        Loads the JS into the local object, and can be called to re-init the obejct data
        @json : object
    */
    init : function(json)
    {
        this.o = json || {};        
        if (typeof json != "undefined") {
            this.o = this.parse(this.o);                           
        }
        return this;
    },  


    /*
        Own passing which does tests on the passed parameter
        Allows for the passing of functions in the payloads,
        Security can be set top stop any anoymous functions from being executed, like from inbound REST calls
    */
    parse : function(value)
    {
        json = value || this.o;

        if (typeof json == "object"){
            //  if a function based object is passed 'internally' as part of the object.
            var func = false;
            JSON.stringify(json, function(k, v){
                if (typeof v === "function"){
                    func = true;
                }
                return v;
            })

            //  no function was found.
            if (!func) {
                function parse(k, v){   
                    if (typeof v == "string" && ((v[0] == "{" || v[0] == "[") && (v[v.length - 1] == "}" || v[v.length - 1] == "]"))) {                                         
                        return JSON.parse(v, this)
                    }               
                    return v;
                }

                try{
                    var j = JSON.parse(JSON.stringify(json, parse));    
                }catch(e){
                    var j = json;
                }                
                json = j;
            }
        
        }else if (typeof json == "string" && ((json[0] == "{" || json[0] == "[") && (json[json.length - 1] == "}" || json[json.length - 1] == "]"))) {
            try{
                var j = JSON.parse(json.replace(/\"\{/g, "{").replace(/\}\"/g, "}"));
            }catch(e){
                var j = json;
            }   
            json = j;           
        }       
        
        return (typeof value != "undefined" ? json : this);
    }, 

    /*
        Set JSON property, can DOT work object
        @path : string - eg. "some.object.property"
    */
    set : function(path, data, json)
    {
        var p = path.split(".");
        var o = json || this.o;
        for (var i = 0; i < p.length -1; i++) {
            if (!o.hasOwnProperty(p[i]) || o[p[i]] === null) {
                o[p[i]] = {};
            }
            o = o[p[i]];
        }
        o[p[p.length - 1]] = data;
        //  ensure that the data is returned, not the OBJECT being set.
        return data;
    },

    /*
        Get JSON property, can DOT work object
        @path : string - eg. "some.object.property"
    */
    get : function(path, json)
    {       
        return path.split('.').reduce(function(a, b) {   
            return (a && typeof a != "undefined" ? (typeof a[b] == "string" && ((a[b][0] == "{" || a[b][0] == "[") && (a[b][a[b].length - 1] == "]" || a[b][a[b].length - 1] == "}")) ? JSON.parse(a[b]) : a[b]) : a);
        }, json || this.o);     
    },


    clone : function(json) 
    {
        return JSON.parse(JSON.stringify(json || this.o));    
    },

    
    merge : function(data, json)
    {
        json = json || this.o;
        for(var key in data) {
            json[key] = data[key];
        } 
        return json;
    },
    

    rekey : function(key, json)
    {
        json = json || this.o;
        if (json instanceof Array){
            var o = {};
            for (var i = 0; i < json.length; i++) {
                if (json[i].hasOwnProperty(key) && typeof json[i][key] != "undefined") {
                    o[json[i][key]] = json[i];                    
                }                
            }
            return o;
        }

        return json
    },

    type : 'OO1JSON'
};