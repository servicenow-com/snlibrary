var OO1Rules = Class.create();
OO1Rules.prototype = {
    
    /*var rules = new OO1Rules({
    "email" : {
        "required" : true,
        "type"     : "email", 
        "regexp"   : "com\.au"                      
    },
    "gender" : {
        "format" : "lower",
        "enum"   : ['male', 'female']
    }
    "deep.scan.object" : {
        "required" : true,
        "enum"   : [200]
    }
    }).data({
        "email" : "somee@mail.com.au",
        "gender" : "male"
    })


    if (!rules.valid()) {
    // show only required field errors.
        gs.info(JSON.stringify(rules.errors(true)))
    // all errors...
        gs.info(JSON.stringify(rules.errors()))
    }else{
        gs.info("Success")
    }*/
    
    o : {},

    /*
        @rules - object
        @data  - object to be validated.
    */
    initialize : function(rules, data) 
    {  
        this.o = {
            "data"      : {},
            "required"  : {
                "fields" : [], 
                "errors" : [],               
            },
            "errors" : {},
            "rules"  : rules || {}
        };       

        if (typeof rules != "undefined") {
             this.rules(rules);            
        }

        if (typeof data != "undefined") {
            this.data(data);
        }
    },


    rules : function(rules)
    {
        if (typeof rules != "undefined") {
            if (rules instanceof Array) {
                this.o.required.fields = rules;
            }else{
                for (var field in rules) {
                    if (rules[field].hasOwnProperty("required") && rules[field].required) {
                        this.o.required.fields.push(field);
                    }
                }
            }  
            return this;
        }
        return this.o.rules;
    },
    
    /*
        Object to be validated.
        @data : json object
    */
    data : function(data) 
    {   
        if (typeof data != "undefined") {
           this.o.data = data; 
           return this;
        }
        return this.o.data;
    }, 

    /*
        Object to ve validated.
        @required : boolean - returns required fields missing.
    */
    errors : function(required)
    {
        if (typeof required != "undefined" && required) {
            return this.o.required.errors;
        } 
        return this.o.errors;
    }, 


    /*
        The validation test
    */
    valid : function()
    {
        this.o.required.errors = [];
        this.o.errors = {};

        for(var field in this.o.rules) {
            //  format the value before being tested.
            if (this.o.rules[field].hasOwnProperty("format")) {
                this.o.data[field] = this.format(field, this.o.rules[field].format);                
            }

            for(var rule in this.o.rules[field]) {

                if (rule == "format") continue;

                var func = rule;                
                //  intercept the 'type' rule, as SN has type as a predefined object key
                if (rule == "type") {
                    func = this.o.rules[field][rule];
                }
                
                var test = this.o.rules[field][rule];

                if (typeof this[func] === "function" || typeof test == "function") {
                   
                    var value = field.split('.').reduce(function(a, b) {    
                        return (typeof a != "undefined" ? a[b] : a);
                    }, this.o.data);     

                    if (func != "required") {
                        if ((!this.o.rules[field].hasOwnProperty("required") || !this.o.rules[field].required) && !value) continue;
                    }
                    
                    var error = false;
                    if (typeof this[func] == "function") {
                        error = !this[func](value, test, field);
                    }else if (typeof test == "function") {
                        error = !test(value, null, field);
                    }
                    
                    if (error) {
                        if (!this.o.errors.hasOwnProperty(field)) {
                            this.o.errors[field] = {} 
                        }
                        this.o.errors[field][rule] = this.o.rules[field][rule];
                    }
                }    
            }            
        }

        //  were any errors detected?
        for(var error in this.o.errors) {
            return false;
        }
        return true;
    },

    
    /*
        @field : String
        @rule  : string
    */
    format : function(field, rule)
    {
        var value = this.o.data[field];
        switch(rule) {
            case "lower" :
                return value.toLowerCase();
            break;
            case "upper" :
                return value.toUpperCase();
            break;
            case "proper" :
                return value.charAt(0).toUpperCase() + value.substr(1).toLowerCase()
            break;
        }
        return value;   
    },


    /*
        @value : String
        @rule  : boolean - should always be true
        @field : string 
    */
    required : function(value, rule, field)
    {
        if (rule === true) {
            if (!value || value == "") {
                this.o.required.errors.push(field);
                return false;
            }
        }        
        return true;
    },


    /*
        @value : String
    */
    string : function(value)
    {
        return (typeof value === "string" ? true : false);        
    },


    //  match value 
    equals : function(value, rule, field)
    {
       return (value === rule ? true : false); 
    },


    /*
        if rule is passed as a string, test the length of the value instead
    */
    max : function(value, rule)
    {
       if (typeof rule == "string"){
            return (value.length <= parseInt(rule) ? true : false);              
       }
       return (parseInt(value) <= rule ? true : false); 
    },


    /*
        if rule is passed as a string, test the length of the 
    */
    min : function(value, rule)
    {
       if (typeof rule == "string"){
            return (value.length >= parseInt(rule) ? true : false);              
       }
       return (parseInt(value) >= rule ? true : false); 
    },



    /*
        @value : String
        @rule  : Array

        NOTE: enum without the quotes invalidates the class and throws an exception.
    */
    "enum" : function(value, rule)
    {
        if (rule instanceof Array && rule.indexOf(value) !== -1) {
            return true;
        }
        return false;
    },
    

    /*
        @value : String
        @rule  : string|array - as array can pass the flag for the exp.
    */
    regexp : function(value, rule)
    {
        var flag = "g";
        if (rule instanceof Array) {
            if (rule.length) {
                if (rule[1]) {
                    flag = rule[1];
                }
                rule = rule[0];
            }
        }
        return new RegExp(rule, flag).test(value);
    },

    
    /*
        @value : String - email address
        @rule  : Array
    */    
    email : function(value, rule)
    {
        if (this.string(value)) {
            var exp = '^(([^<>()\\[\\]\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$';
            return (this.regexp(value.toLowerCase(), (rule === true || rule == "email" ? exp : rule)));
        }
        return false;
    },

    /*
        Must mathc a certain length
    */
    length : function(value, rule)
    {
        return (value.length == parseInt(rule));
    },


    
    type : 'OO1Rules'    
};