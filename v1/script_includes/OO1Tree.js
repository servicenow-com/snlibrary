var OO1Tree       = Class.create();
OO1Tree.prototype = Object.extendsObject(OO1Base, {
    
    o : {},
    
    initialize: function(properties) 
    {	
        OO1Base.prototype.initialize.call(this, (function(o){            
            //  pass any defaults options.
            if (typeof properties != "undefined"){
                if (typeof properties == "object") {
                    for (var key in properties) o[key] = properties[key];                
                }else{
                    o.table = properties;
                }  
            }
            return o;       
        })({
            "child"     : "sys_id",
            "parent"    : "parent",
            "registry"  : [],
            "table"     : "cmdb_rel_ci",
            "query"     : "",            
            //
            "mappings"  : {}
        }));          
    },


    data : function(data)
    {
        if (typeof data == "object") {
            for(var k in data) {
                this.o.mappings[k] = data[k];
            }
        } 
    },


    /**
    *   Retrieve the categories of the passed parent_id
    *   @category_id: String
    *   @return     : Array
    */
    parents : function(category_id)
    {
        var self = this;
        var db = new GlideRecord(this.o.table);      
        db.addEncodedQuery(this.o.child +"="+ category_id + (function(){
            if (self.o.query != "") {
                return "^"+ self.o.query.replace(/^\^/, '');
            }
            return "";
        })());
        db.query();
        
        var a = [];
        while (db.next()) {

            a.push(this.map(this.o.mappings, db));                       
            //
            a.unshift.apply(a, this.parents(db.getValue(this.o.parent)));
        }
        return a;
    },

    /**
    *   Retrieve the categories of the passed parent_id
    *   @parent_id  : String
    *   @maxdepth   : Int - limit the search. 0 would equal the cats of the passed parent only, no cascading
    *   @depth      : Int - Current depth    
    *   @return     : Boolean
    */
    children : function(parent_id, maxdepth, _depth)
    {   
        var self = this;
        if (typeof(_depth) == "undefined") {
            //
            this.o.mappings[this.o.parent] = this.o.parent;
            //
            _depth = 1;
        }
        
        var db = new GlideRecord(this.o.table);      
        db.addEncodedQuery(this.o.parent +"="+ (typeof parent_id == "boolean" ? "" : parent_id) + (function(){
            if (self.o.query != "") {
                return "^"+ self.o.query.replace(/^\^/, '');
            }
            return "";
        })());

        db.query();

        var a = [];
        while (db.next()) {
            // ensure that we don't 'loop' due to a parent existing as a child
            if (this.o.registry.indexOf(db.getValue(this.o.mappings.sys_id || "sys_id")) !== -1 ) {
                continue;
            }
            //  setup data object
            var o   = this.map(this.o.mappings, db, {"sys_id" : db.getValue("sys_id")});                        
            o.depth = _depth;
            //
            a.push(o);
            //
            this.o.registry.push(this.o.mappings.sys_id);
            
            if (typeof maxdepth == "undefined" || _depth < maxdepth) {
                a.push.apply(a, this.children(db.getValue(this.o.mappings.sys_id || "sys_id"), maxdepth, _depth + 1));
            }           
        }
        return a;
    },


     type: 'OO1Tree'
});