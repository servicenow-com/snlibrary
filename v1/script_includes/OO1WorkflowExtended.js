var OO1WorkflowExtended = Class.create();
OO1WorkflowExtended.prototype = Object.extendsObject(OO1Workflow, {

    /*    
    Note: Class designed to be called from within Workflows and activities 

    Caution: Defined variables in workflow activities are brought into class scope and 
    will cause issues within these method as defined variables in activities are global in scope.
    - Variable 'wf' is an example. 

    Note: 'current' and 'worflow' variable are global, and brought into class scope.

    */
    
    o : {},
    
    initialize: function(properties)
    {
        OO1Workflow.prototype.initialize.call(this, (function(o){            
            //  pass any defaults options.
            if (typeof properties == "object") {
                for (var key in properties) o[key] = properties[key];                
            }  
            return o;       
        })({
            "init"  : (properties.init || false),   
        }));  

        if (this.o.init) this.init(this.o.init);
    },


    //  init anything that's needed in the workflow 
    init : function(data)
    {
        
        return this;       
    },  
    
 
    type: 'OO1WorkflowExtended'
});