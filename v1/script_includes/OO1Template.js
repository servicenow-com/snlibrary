var OO1Template = Class.create();
OO1Template.prototype = Object.extendsObject(OO1Base, {
    /*
        Example for extending the Base class.
        - ensure the the 'o' object is declared.
    */
    o : {},
    
    initialize : function(properties) 
    {  
        OO1Base.prototype.initialize.call(this, (function(o){            
            //  pass any defaults options.
            // if (typeof properties == "object") {
            //     for (var key in properties) o[key] = properties[key];                
            // }  
            return o;       
        })({
            "data" : properties || current
        }));
    },

    /*
        parse string content to be translated.
        @content : string || array (current record fields)
    */
    parse : function(content, object)
    {
        var self = this;        
        var o    = object || self.o.data;
        if (o) {
            if (content instanceof Array) {
                for (var i = 0; i < content.length; i++) {
                    if (this.o.data.hasOwnProperty(content[i])) {
                        this.o.data[content[i]] = this.parse(this.o.data[content[i]], object);
                    }                
                }
                return (this.o.data instanceof GlideRecord || this.o.data instanceof GlideRecordSecure ? this : this.o.data);
            }      
        
            return content.replace(/\$\{(.*?)\}/g, function(match, path) {
                var v = path.split('.').reduce(function(c, b) {   
                    var k = (b.indexOf("$") === 0 ? b.substring((b.indexOf("$") === 0 ? 1 : 0), b.length) : b);
                    return (c ? (b.indexOf("$") === 0 ? c[k].getDisplayValue() : c[k]) : c);        
                }, o);            
                // return value or empty value if field was found, if not show dot walk error
                return (v ? v : (typeof v != "undefined" ? "" : match)); 
            });
        }
        // No object was parse.
        return false;
    },


    /*
        used to update the SN record for "after" business rules etc...
        Used for the purpose of chaining from the above parse() method CLASS.parse(['field_name']).update()
    */
    update : function(table, id, security)
    {
        if (this.o.data instanceof GlideRecord || this.o.data instanceof GlideRecordSecure) {
            this.o.data.update();
        }else{
            // DB Update - Todo...
            if (typeof this.DB == "function") {
                // this.DB(table).update();
                // gs.info("DB")
            }
        }
    },

    type : 'OO1Template'    
});