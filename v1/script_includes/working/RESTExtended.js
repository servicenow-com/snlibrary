var RESTExtended = Class.create();
RESTExtended.prototype = Object.extendsObject(REST, {
   
    initialize: function(properties) 
    {
        var self = this;

        REST.prototype.initialize.call(this, (function(o){    
            //  pass any defaults options.
            if (typeof properties == "object") {
                for (var key in properties) o[key] = properties[key];                
            }  
            return o;       
        })({
            "mappings"  : {
                // "DBfield1" : "payload.field"
                // "DBfield2" : (function(){self._mapping()})
            },
            "response" : true
        })); 

        //
        if (this.validate()) {
            // output the response result
            if (this.property("response")) {
                this.response(this.process());        
            }    
        }

        
    },

    /*
        Validate the request.
    */
    validate : function()
    {
        return true;
    },

    /*
        Placeholder for a callback on a field within the mapping object
    */
    _mapping : function()
    {
        // this.payload("payload.field");
        // this.data("DBfield1");
    },

    /*
        Business logic based on the inbound request.
    */
    process : function()
    {
        // this.status("403")
        // this.message("This is the message for the transaction record")
        // this.error("output error message for response")
        // return {"success" : true}
    },
   
    type : 'RESTExtended'
});