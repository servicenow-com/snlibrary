var REST = Class.create();
REST.prototype = Object.extendsObject(Base, {
    
    initialize: function(properties) 
    {
    	Base.prototype.initialize.call(this, (function(o){            
            //  pass any defaults options.
            if (typeof properties == "object") {
                for (var key in properties) o.properties[key] = properties[key];                
            }  
            return o;       
        })({
            "config" : {
            	"library" : {
            		"headers" : "setHeaders",
            		"status"  : "setStatus",
            		"data" 	  : "setBody",
            	},
            	//  API Transaction table.
                "transactions" : {
                    "table"   : "u_api_transaction",
                    "request" : {
                        "method"  : "u_method",
                        "headers" : "u_headers",
                        "url"     : "u_url",
                        "data"    : "u_data",   
                        "name"    : "u_name", 
                        "type"	  : "u_type",
                        "state"   : "u_status"                       
                    },
                    "response" : {
                        "headers" : "u_response_headers",
                        "status"  : "u_status",
                        "body"    : "u_response",
                        "message" : "u_response_message",
                        "state"   : "u_state"
                    }                  
                },
                // Script REST resource
                "rest" : {
	                "table"  : "sys_ws_operation",
                    "resource" : {
                    	"name" 	 : "name",
                        "url"    : "operation_uri",                        
                        "method" : "http_method",
	               	}              
	            }
            },
            "request" : {
            	"name"  	: "",
            	"type"		: "inbound",
            	"method"  	: "GET",
            	"state"		: "",
                "headers" 	: (function(){
                	var  o = {};
                	var a = ["Accept", "Content-Type"];
                	for (var i = 0; i < a.length; i++) {
                		var h = request.getHeader(a[i]);
                		if (h) {
                			o[a[i]] = h;
                		}
                	}
                	return o;
                })(),
                "url"		: "",
               	"data"    	: (function(){
                    //  Stops fatal error when trying to access body.data on a GET request
                    try{
                        if (request.getHeader("Content-Type") == "application/json") {
                            if (typeof request.body.data == "string") {                           
                                if (request.body != "") {
                                    return JSON.parse(request.body.data);
                                }
                                return {};
                            }
                            return request.body.data;
                        }
                    }catch(e) {
                        return "";
                    }

                })(),
                "segments" 	: request.pathParams,
                "query"    	: request.queryParams,
                "resource"  : (typeof properties != "undefined" && properties.hasOwnProperty("sys_id") ? properties.sys_id : false)                     
            },
            //  response object
            "response" : {
            	"headers" : {
                    "Content-Type" : "application/json"
                },
                "status"  : 200,
                "data"    : {},
                "message" : "",
                "state" : "success"              
            },
            //  Glide Record of the logged transaction 
            "transaction" : null,
            //  Glide Record of the REST resource
            "rest" : null,
            "properties"  : {
            	//	this stores the sys_id of the Scripted REST record.
            	"sys_id" : null,
                //  Log into a transaction table. Cache will use this table.
                "log" : false, 
				//	place-holder for extended mappings
                "mappings" : null                
            },           
            "data" : null
        })); 
        
    	//	setup the data for the transaction table.
    	if (this.o.request.resource) {
        	var db = new GlideRecord(this.o.config.rest.table);
        	if (db.get(this.o.request.resource)) {
				//	
        		for(var key in this.o.config.rest.resource){
        			var v = db[this.o.config.rest.resource[key]].getValue();
        			//
        			if (key == "url" && this.o.request.segments) {
        				for(var k in this.o.request.segments){
        					v = v.replace(new RegExp("\\{"+ k +"\\}", 'g'), this.o.request.segments[k]);
        				}       
        				v = "https://"+ this.getProperty("instance_name") +".service-now.com" + v; 				
        			}	
        			this.o.request[key] = v;
        		}
        		//
        		this.o.rest = db;        		
        	}
        }

        //
        if (this.o.properties.log) {
        	var o = {};
        	for (var f in this.o.request) {
	    		if (this.o.config.transactions.request.hasOwnProperty(f)){
	    			o[this.o.config.transactions.request[f]] = (function(self){                        
                        if (f == "state" || f == "method") {
                            return self.o.request[f].charAt(0).toUpperCase() + self.o.request[f].slice(1).toLowerCase();
                        }
                        return self.o.request[f];	    			
                    })(this);
	    		}	    	
	    	}
        	this.transaction(o);
        }  

        //  automatically transform the data if any mappings have been requested.
        if (this.property("mappings")) {
        	this.o.data = this.transform();
        }
    },


    /*
    *   payload request
    */
    payload : function(path)
    {
        if (typeof path != "undefined") {
           	return this.JSON(this.o.request.data).get(path)            
        }
        return this.o.request.data;
    },


    /*
    *   data object that has the transformed payload.
    */
    data : function(path, value)
    {
        if (typeof path != "undefined") {
            if (typeof set != "undefined"){
                return this.JSON(this.o.data).set(path, value);            
            }
           	return this.JSON(this.o.data).get(path)            

        }
        return this.o.data;
    },


    /*
	*	Get a header from the request 
    */
    header : function(key)
    {
    	if (this.o.request.headers.hasOwnProperty(key)) {
    		return this.o.request.headers[key].toLowerCase();
    	}
    	return null;
    },

    
    /*
	*	Get uri segment of the URL 
    */
    segment : function(key)
    {
    	if (this.o.request.segments.hasOwnProperty(key)) {
    		return this.o.request.segments[key].toLowerCase();
    	}
    	return null;
    },

    
    /*
    *   Get Query segment of the URL 
    */
    query : function(key)
    {
        if (this.o.request.query.hasOwnProperty(key)) {
            return this.o.request.query[key][0];
        }
        return null;
    },


    /*
    *   Set the status of the REST response
    */
    status : function(status)
    {
        if (parseInt(status) >= 200 && parseInt(status) < 400) {
            this.o.response.state = "success";          
        }else{
            this.o.response.state = "failed";          
        }

        //
        this.o.response.status = status;

        return this;
    },


    /*
    *   Set the error message of the REST response
    */
    error : function(error)
    {       
        this.o.response.data.error = (typeof error != "undefined" ? error : "Bad request. Requested URL was not found or the URL contains incorrect segments.");
        
        return this;
    },


    /*
    *   Set the message to the end user
    */
    message : function(message, output)
    {
        // db insert value
        this.o.response.message = message;
        //  should this be part of the output of the data response
        if (typeof output != "undefined" && output) this.o.response.data.message = message;

        return this;
    },

    
    /*
    *   Call to output the response to the requestor
    */
    response : function(data)
    {   
        if (typeof data != "undefined" && data != null) {
            this.o.response.data.data = (typeof data != "object" ? JSON.parse(data) : data); 
        }                

        for (var key in this.o.config.library) {
            if (this.o.response.hasOwnProperty(key)) {
                if (this.o.response[key]) {
                    response[this.o.config.library[key]](this.o.response[key]);                    
                }
            }
        }

        //  update the transaction record with the outputted response data.
        if (this.o.transaction) {
            var o = {};
            for (var f in this.o.config.transactions.response) {
                if (this.o.response.hasOwnProperty(f)) {
                    o[this.o.config.transactions.response[f]] = (function(self){                        
                        if (f == "state" || f == "method") {
                            return self.o.response[f].charAt(0).toUpperCase() + self.o.response[f].slice(1).toLowerCase();
                        }
                        return self.o.response[f];                   
                    })(this);                   
                }
            }
            this.transaction(o);
        }
    },


    /*
    * Log the inbound transaction
    */
    transaction : function(data)
    {       
        if (this.o.properties.log && typeof data != "undefined") {
            var db = new GlideRecord(this.o.config.transactions.table);           
            if (this.o.transaction) {
                if (db.get(this.o.transaction.getUniqueValue())) {                  
                    for (var f in data) {
                        db[f] = (typeof data[f] == "object" ? JSON.stringify(data[f]) : data[f]);                       
                    }       

                    if (db.update()) {
                        this.o.transaction = db;
                    }
                }               
            }else{          
                for (var f in data) {
                    db[f] = (typeof data[f] == "object" ? JSON.stringify(data[f]) : data[f]);                       
                }       
                if (db.insert()) {
                    this.o.transaction = db;
                }
            }
        }

        return this.o.transaction;
    },


    /*
        Transform the payload data into a database object
    */
    transform : function()
    {
        return this.map(this.property("mappings"), this.o.request.data);        
    },

    type : 'REST'
});