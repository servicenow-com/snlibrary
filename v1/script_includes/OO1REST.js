var OO1REST = Class.create();
OO1REST.prototype = Object.extendsObject(OO1API, {

    //  decalared here to ensure that the scope for object works correctly.
    o   : {},
    // OO1API
    // OO1DB
    // OO1Rules
    // OO1JSON
        
    initialize: function(properties) 
    {
        // Note this parent class this is called within the transaction method
        OO1API.prototype.initialize.call(this, (function(o){            
            //  pass any defaults options.
            if (typeof properties == "object") {
                //  lasy passing of options. See if additional properties other than 'options'
                //  allows to lazy pass only the 'options' to the API when being extended.
                var prefix = "option.";
                for (var key in properties) {
                    if (key.indexOf(".") !== -1 || o.hasOwnProperty(key)) {
                        prefix = "";
                        break;
                    }
                }
                for (var key in properties) o[(prefix + key)] = properties[key];     
            }  
            return o;    
        })({
            "transaction.mappings.fields.u_type"     : "@inbound",
            "transaction.mappings.fields.u_resource" : (function(){
                if (properties.hasOwnProperty("resource_id") || (properties.hasOwnProperty("option.resource_id"))) {
                    return "@"+ (properties.resource_id ? properties.resource_id : properties["option.resource_id"]);
                }
                return null;
            })(),
            "config" : {
                "library"  : "RESTEndpoint",
                "mappings" : {
                    "RESTEndpoint" : {
                        "headers" : "setHeaders",
                        "status"  : "setStatus",
                        "data"    : "setBody"
                    }                    
                },
                "request" : null,                
            }, 
            "resource" : {
                "table"  : "sys_ws_operation",
                "mappings" : {
                    "name"   : "name",
                    "url"    : "operation_uri",                        
                    "method" : "http_method",
                },
                "sys_id" : null              
            }, 
            "response" : {
                "headers" : {
                    "Accept"       : "application/json",
                    "Content-Type" : "application/json"
                },
            },                        
            "request" : {
                "headers"   : (function(){
                    try{
                        var o = {};
                        var a = ["Accept", "Content-Type"];
                        for (var i = 0; i < a.length; i++) {
                            var h = request.getHeader(a[i]);
                            if (h) {
                                o[a[i]] = h;
                            }
                        }
                        return o;
                    }catch(e) {
                        return null;
                    }
                })(),                                
                "data" : (function(){
                    // Stops fatal error when trying to access body.data on a GET request
                    try{
                        if (request.getHeader("Content-Type") == "application/json") {
                            return new OO1JSON().parse(request.body.data);                           
                        }                        
                    }catch(e) {
                        return null;
                    }

                })(),
                "segments"  : (function(){
                    try{ return request.pathParams }catch(e){return null};
                })(),
                "query"  : (function(){
                    try{ return request.queryParams }catch(e){return null};
                })()                
            },
            //  stores the Scripted REST Resource sys_id. Used to be attached to the transaction record.
            "option.resource_id"    : null,
            //  allow SN library to handle output of the response 'data' object      
            "option.body"           : true,            
            //  are any rules passed?
            "option.rules"          : null,            
        }));

        // //   are we to log this transaction?
        if (this.oo("option.log") == true && this.oo("option.resource_id")) {
            //
            this.oo("resource.sys_id", this.oo("option.resource_id"))             
            //
            this.transaction();       
        }        

        if (this.oo("option.rules")) {
            //  bring in rules library
            this.OORules = new OO1Rules(this.oo("option.rules"), this.oo("request.data"));

        }
        //  this runs the script, and then runs the success or failure. 
        this.validate();
    },


    /*
        Validate the request.
    */
    validate : function(rules)
    {
        if (this.OORules) {
            if (!this.OORules.valid()){
                return this.failure(this.OORules.errors(), "Validation error");        
            }    
        }
        return this.success(this.oo("request.data"));
    },



    /*
        Extend the transation method.
    */
    transaction : function()
    {
        var self = this;
        // create the transaction.
        if (!this.oo("transaction.sys_id")) {
            var data = new OO1DB(this.oo("resource.table"), false).data((function(){
                var m = self.oo("resource.mappings");
                if (m.hasOwnProperty("url")) {
                    var field = m.url;
                    m.url = (function(db){
                        // replace the segments in the uri with the passed values
                        var uri = db[field].getValue();
                        if (self.oo("request.segments")) {
                            var o = self.oo("request.segments");
                            for (var k in o) {
                                uri = uri.replace(new RegExp("\\{"+ k +"\\}", 'g'), o[k]);
                            }
                        }
                        return self.OO_URL + uri;
                    })
                }
                return m;
            })()).single("sys_id="+ this.oo("resource.sys_id"));
                        
            if (data) {
                for(var field in data) {
                    this.o.request[field] = data[field];
                }
                //  run the parent to create the transaction.
                return OO1API.prototype.transaction.call(this);                 
            }        
        }else{
            //  update the transaction with the result.
            //  swap the mapping object keys values around for mapping
            var data = this.map((function(map){
                var o = {};
                for (var k in map) o[map[k]] = k;
                return o;
            })(this.o.transaction.mappings.response), this.o.response, data);
            
            new OO1DB(this.oo("transaction.table"), false).update(data, "sys_id="+ this.oo("transaction.sys_id"))
        }
    },



    /*
    *   Set the status of the REST response

    */
    status : function(status)
    {
        this.oo("response.state", (parseInt(status) >= 200 && parseInt(status) < 400 ? "success" : "failed"))
        this.oo("response.status", status);

        if (this.oo("status."+ status)){
            if (parseInt(status) < 200 || parseInt(status) >= 400) {
                this.error(this.oo("status."+ status));
                if (!this.oo("response.message") || this.oo("response.message") == "") {
                    this.oo("response.message", this.oo("status."+ status));
                }
            }else{
                if ((!this.oo("response.message") || this.oo("response.message") == "") && (!this.oo("response.data.message") || this.oo("response.data.message") == "")) {
                    this.message(this.oo("status."+ status), true);    
                }else if (!this.oo("response.data.message") || this.oo("response.data.message") == "") {
                    this.oo("response.data.message", this.oo("status."+ status));    
                }
            }                       
        }
        return this;
    },


    /*
    *   Get uri segment of the URL 
    */
    segment : function(key)
    {
        if (this.o.request.segments.hasOwnProperty(key)) {
            return this.o.request.segments[key].toLowerCase();
        }
        return null;
    },


    
    /*
    *   Set the error message of the REST response
    */
    message : function(message, data)
    {       
        this.oo("response.message", message);        
        if (typeof data != "undefined" && data == true) {
            this.oo("response.data.message", (data === true ? message : data));    
        }        
        return this;
    },

    /*
    *   Set the error message of the REST response
    */
    error : function(error)
    {       
        this.oo("response.data.error", error);
        return this;
    },


    //  respond to the request.
    response : function(data)
    {
        if (typeof data != "undefined") {
            this.oo("response.data.data", data)
        }

        //  response to the SN librrary thats passed in the Scripted Rest record.
        var mappings = this.oo("config.mappings."+ this.o.config.library);
        for(var key in mappings) {
            if (this.o.response.hasOwnProperty(key)) {
                if (key == "data" && (this.oo("debug") || !this.oo("option.body"))) {
                    continue;
                }
                //  ensure the SN global REST variable exists.
                if (typeof response != "undefined") response[mappings[key]](this.o.response[key]);                    
            }
        }

        //  
        if (this.oo("transaction.sys_id")){
            //  update the transaction with the response.
            this.transaction()    
        }
        
        //  for development only, setData will handle the output.
        return this.oo("response");
    },
    


    // place holders
    success : function(data, status)
    {
        this.status(typeof status != "undefined" ? status : 200).response(data);
    },


    failure : function(data, error, status)
    {
        this.status(typeof status != "undefined" ? status : 400);
        //
        if (typeof error != "undefined" && error !== null) {
            this.error(error);
        }
        
        if (typeof data != "undefined" || data == null){
            this.response();     
        }else{
            this.response(data);    
        }
    },
   

    

    type : 'OO1REST'
});