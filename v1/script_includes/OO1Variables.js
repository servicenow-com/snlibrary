var OO1Variables       = Class.create();
OO1Variables.prototype = Object.extendsObject(OO1Base, {
    /*
        Ideally only used within the context of a workflow.
    */
    o : {},

    initialize: function(properties) 
    {	
        var self = this;
        //
        OO1Base.prototype.initialize.call(this, (function(o){            
            //  pass any defaults options.
            if (typeof properties != "undefined"){
                if (typeof properties == "object") {
                    for (var key in properties) o[key] = properties[key];                
                }  
            }
            return o;       
        })({
            // within the frame work of the init. RITM
			"variables" : (function(){
                if (current && current.hasOwnProperty("variables")) {
                    return self.set(current.variables, true);
                }
                return null;
            })(),
            //  passed properties
            "ignore"    : [],
            "seperator" : ": ",
            "blank"     : "-",
            "blanks"    : true,            
		}));  

        //
        if (!this.oo("variables")) {
            this.set(this.get());
        }
    },


    set : function(variables, init)
    {
        var o = {};        
        var f = null;
        for(var field in variables) {
            //  ignore unclassed variables, like variable sets
            if (variables[field].getQuestion().getType() && this.o.ignore.indexOf(field) === -1) {                
                //
                if (!this.oo("blanks") && variables[field].getDisplayValue() == "") continue;
                //
                o[field] = {
                    "label"   : variables[field].getQuestion().getLabel(),
                    "value"   : variables[field].getValue(),
                    "display" : variables[field].getDisplayValue(),
                    "type"    : variables[field].getQuestion().getType(),
                    "table"   : (variables[field].hasOwnProperty("getListTable") ? variables[field].getQuestion().getListTable() : null),
                    // "order"   : variables[field].getQuestion().getOrder()
                };
                //  flag to state a variable key was found;
                f = true;
            }
            
        }

        if (typeof init != "undefined" && init){
            return (f ? o : null);    
        } 
        //
        return this.oo("variables", (f ? o : null));        
    },


    /*
    *   Get the variables from the parent RITM 
    *   @request_id : string - sys_id of the request
    */
    get : function(request_id)
    {
        return new OO1DB("sc_req_item", false).single("request="+ (typeof request_id != "undefined" ? request_id : current.request.getValue()) +"^ORDERBYsys_created_on").variables;
    },


    /*
    *   Builds a variable data array    
    */
    data : function(ignore, callbacks, blanks)
    {
        var a = [];        
        var self = this;
        if (this.oo("variables")) {
            var vars = this.oo("variables");            
            for(var fld in vars) {
                //  ignore
                if (typeof ignore != "undefined" && ignore instanceof Array && ignore.indexOf(fld) !== -1) continue;
                if (typeof blanks != "undefined" && blanks === false && vars[fld].display == "") continue;
                //  callback to build data?
                if (typeof callbacks == "object" && callbacks.hasOwnProperty(fld)) {
                    a.push(callbacks[fld](vars[fld], current.variables[fld]));
                }else{
                    a.push(vars[fld].label + this.oo("seperator") + (function(){
                        if (vars[fld].display == "") {
                            return self.oo("blank");
                        }
                        switch (String(vars[fld].type)) {
                            //  Yes/No
                            case "7" :
                                return (vars[fld].value == "false" ? "No" : "Yes");
                            break;
                        }
                        return vars[fld].display;
                    })());
                }
            }  
        }
        return a;
    },


     type: 'OO1Variables'
});