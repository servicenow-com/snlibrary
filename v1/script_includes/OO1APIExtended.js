var OO1APIExtended = Class.create();
OO1APIExtended.prototype = Object.extendsObject(OO1API, {
    
    o : {},
    
    initialize: function(properties) 
    {
        OO1API.prototype.initialize.call(this, (function(o){            
            var prefix = "option.";
            for (var key in properties) {
                if (key.indexOf(".") !== -1 || o.hasOwnProperty(key)) {
                    prefix = "";
                    break;
                }
            }
            for (var key in properties) o[(prefix + key)] = properties[key];               

            return o;       
        })({
            //
            "endpoints" : {
                // "name" : {
                    // "host"      : "",
                    // "segments"  : "",
                    // "headers"   : {}
                    // "auth" : 'oauth2',
                    // "profile" : "sadasd"
                    // "username" : null,
                    // "password" : null
                //}
            },

            //  How many times should the API retry before complelty failing.
            // "option.retries"   : 3,
            //  how long the api should wait before retrying Milliseconds
            // "option.wait"      : 1000,            
            //  how offen is the cache refreshed? Milliseconds
            // "option.cache"     : false,
            // "option.refresh"   : 3000,
            //  Log into a transaction table. Cache will use this table.
            // "option.log"       : false,
            //  what endpoint are we to use, if there are multiple.
            // "option.endpoint"  : "1.0",
            // "option.endpoint"  : "fast",            
            // "option.auth"  : null,
            // "option.username"  : null,
            // "option.password"  : null
            // "option.profile" : ""
        }));
    },


    /*
        endpoint: request          
    */
    postAPIExtendedMethod : function(data, success, failed)
    {
        var self = this;
        this.post({
            "name"    : (function(){
                switch(self.o.option.endpoint){
                    default :
                        return "Name for the transation logging table";
                    break;
                }
            })(),  
            // "segments"  : (function(){
            //      switch(self.o.option.endpoint){
            //         case "egress" :
            //             return self.oo("registry.endpoint.segments") +"/whitelist/submit";
            //         break;
            //         default :
            //             return self.oo("registry.endpoint.segments");
            //         break;
            //     }                
            // })(),
            "data"    : data,
            "success" : success,
            "failed"  : failed
        });
    },

 
    type: 'OO1APIExtended'
});