var OO1Extended = Class.create();
OO1Extended.prototype = Object.extendsObject(OO1Base, {
    /*
        Example for extending the Base class.
        - ensure the the 'o' object is declared.
    */
    
    o : {},
    
    initialize : function(properties) 
    {  
        OO1Base.prototype.initialize.call(this, (function(o){            
            //  pass any defaults options.
            if (typeof properties == "object") {
                for (var key in properties) o[key] = properties[key];                
            }  
            return o;       
        })({
            "name"      : "Parent",
            "parent"    : "one",            
            "data"    : {"one" : "1", "two" : "2"}            
        }));

        this.info(this.o)          
    },

    method : function()
    {
        // this.info(this.type)       
    },

    type : 'OO1Extended'    
});