var OO1Base        = Class.create();
OO1Base.prototype  = {    
    /*
        Base class - for all 'OO' extended classes  
    */
        
    //  private object for extended class properties
    //  must be decleared in the extended classes otherwise 
    //  the object scope will be not be inherited correctly.
    o   : {},
    //  PRODUCTION cname value from the URL for the servicenow domain
    //  Used to determine what environment is being used
    cname  : gs.getProperty("oo.environment.cname")  || "devxxxxx",
    domain : gs.getProperty("oo.environment.domain") || "service-now.com",

    initialize : function(o) 
    {
        //  used for DEV development.
        var devs = [
            '6816f79cc0a8016401c5a33be04be441'
        ];

        this.o = {
            "debug" : this.getProperty("oo.debug") || false          
        };
        //  create the JSON object first as this is used to all 'property' lookups
        //  Define class required classes
        this.OOJSON = new OO1JSON();         
        
        //  Create a local protected object to store variables
        if (typeof o == "object") {
            if (o.hasOwnProperty("option.debug")) {
                this.o.debug = o["option.debug"];
                delete o["option.debug"];                
            }

            var a = [];
            for (var k in o) {
                //
                this.oo(k, o[k])    
                //  remove the 'dot walked' set elements in the passed 'o'                            
                if (k.indexOf(".") !== -1) {
                    a.push(k);
                }
            }
            //  remove as they've been added to the object
            for (var i = 0; i < a.length; i++) {
                delete o[a[i]];
            }
        } 
        
        //  Define class CONSTANTS
        this.OO_DEBUG     = this.o.debug;
        this.OO_INSTANCE  = this.getProperty("instance_name");          
        this.OO_URL       = "https://"+ this.OO_INSTANCE +"."+ this.domain;
        this.OO_ENV       = (function(self){            
            //  this will need customisation based on the domain and cname.
            switch(self.OO_INSTANCE.replace(self.cname, "").toLowerCase()) {
                case "dev" :
                    return "DEV";
                break;
                case "uat"  :
                case "test" :
                    return "TEST";
                break;
                default :
                    return "PROD";
                break;
            }        
        })(this);
        //  Used for development only. Can be used to skip approvals and wait scripts
        //  to speed up development.  Set the developers sys_id. Only runs in DEV
        this.OO_DEV = (this.OO_ENV == "DEV" && devs.indexOf(gs.getUserID()) !== -1);
    },

    /*
       return envirnoment.
    */
    environment : function(env)
    {
        if (typeof env == "string") {
            return (this.OO_ENV == env.toUpperCase());
        }
        return this.OO_ENV;
    },

    /*
        See class Json();
        @data : json object       
    */
    JSON : function(data)
    {        
        if (data) {
            return this.OOJSON.init(data);          
        }
        return this.OOJSON;
    },


    //
    DB : function(table, security)
    {
        if (typeof table != "undefined") {
            this.OODB = new OO1DB(table, security);            
        }
        return this.OODB;
    },
   

    /*
        Method that works with the private 'o' object
    */
    oo : function(path, set)
    {
        if (typeof set != "undefined") {
            return this.JSON().set(path, set, this.o);           
        }
        return this.JSON().get(path, this.o);    
    },


       
    /*
        Handles objects better that the SN version.
    */   
    log : function(data, source)
    {
        if (typeof data == "object") {
            data = JSON.stringify(data, null, "\t");          
        }
        gs.log(data, (typeof source != "undefined" ? source : this.type));
    },
    
    /*
        Replaces the SN getProperty method to return the correctly set data type
        @name : string - system property name
        @otherwise : anything - return if property wasn't found.
    */
    getProperty : function(name, otherwise)
    {
        var db = new GlideRecord("sys_properties");
        if (db.get("name", name)) {
            //
            switch(String(db.type)){
                case "integer" :
                    return parseInt(String(db.value));
                break;
                case "boolean" :
                    return (String(db.value) == "true" ? true : false);
                break;
            }
            return String(db.value);
        }
        //  not found
        return (typeof otherwise != "undefined" ? otherwise : null);
    },

   
    /*
        Set a property ... scope issue 
    */
    setProperty : function(name, value)
    {
        var db = new GlideRecord("sys_properties");
        if (db.get("name", name)) {
            db[name] = value;
            if (db.update()) {
                return value;
            }
        }else{
            db[name] = value;
            if (db.insert()) {                
                return value;
            }
        }
        return null;        
    },

        


    /*
    *   Basic mapping of data into an object
    *   @mappings : object || comma sep string - use shorthand symbols for ease of use.
    *       @ - supress any functionality and return the string value
    *       $ - return .getDisplayValue() if GlideRecord passed. (default none is .getValue())                        
    *   @data : object | @GlideRecord
    *   @object : add to existing passed object
    */
    map : function(mappings, data, object)
    {
        var self = this;
        var obj = object || {};
        // 
        if (typeof mappings == "string") {
            // convert to object > Array
            mappings = mappings.split(",");
        }

        if (typeof mappings == "object") {            
            if (mappings instanceof Array) {
                var o = {};
                for (var i = 0; i < mappings.length; i++) {
                    o[mappings[i]]= mappings[i];
                }    
                mappings = o;
            }

            for (var key in mappings) {
                if (typeof mappings[key] == "function") {
                    var response = mappings[key](data, key, this);  
                    if (typeof response != "undefined" && response != null) {
                        obj[key] = response; 
                    }
                }else if (key == "*" && (data instanceof GlideRecord || data instanceof GlideRecordSecure)) {
                    var fields = data.getFields();
                    for (var i = 0; i < fields.size(); i++) {
                        var field = fields.get(i);
                        var type = String(field.getED().getInternalType()).toLowerCase();
                        var key  = String(field.getName());
                        if (data.hasOwnProperty(key)) {
                            switch (String(type)) {
                                case "glide_date" :
                                case "glide_date_time" :
                                    obj[key] = data[key].getDisplayValue();
                                break;
                                case "boolean" :
                                    obj[key] = (data[key].getValue() == "1" ? true : false);
                                break;
                                default :                                         
                                    obj[key] = data[key].getValue();                                    
                                break;
                            }           
                        }
                    } 
                    break;                                   
                }else{
                    var value = (function(reference){                           
                        //  force empty key
                        if (typeof reference === "string" && reference.length == 0) return "";
                        if (typeof reference === "boolean") return reference;
                        if (reference === null) return reference;
                        //  supress any functionality and just return the string value.
                        if (typeof reference === "string" && reference.indexOf("@") === 0) return reference.substring(1, reference.length);                                                        
                        //
                        if (data instanceof GlideRecord || data instanceof GlideRecordSecure) {
                            if (reference.indexOf(".") === -1) {
                                if (!data.hasOwnProperty(reference.replace("$", ""))) return null;
                            }
                            var element = data.getElement(reference.substring((reference.indexOf("$") === 0 ? 1 : 0), reference.length));                                                                                        
                            return (reference.indexOf("$") === 0 ? 
                                (function(){
                                    var v = element.getValue();
                                    if (v == "null" || v == null) {
                                        return "";
                                    }
                                    return element.getDisplayValue();                                    
                                })() : 
                                (function(){
                                    var v = element.getValue();
                                    if (v == "true" || v == "false") {
                                        return (v == "true");
                                    }

                                    if (v !== null && (v.indexOf("[") === 0 || v.indexOf("{") === 0) && (v[v.length -1] === "}" || v[v.length -1] === "]")) {
                                        return JSON.parse(v);
                                    }

                                    return (v == "null" || v == null ? "" : v);
                                })()
                            );                              
                        }else if(typeof reference == "object") {
                            return reference;
                        }                        
                        return self.JSON(data).get(reference);
                    })(mappings[key]);

                    //
                    if (typeof value != "undefined" && value != null) {
                        obj[key] = value; 
                    }                    
                }      
            }
        }

        return obj;
    },

    type: 'OO1Base'
};