var OO1Cart       = Class.create();
OO1Cart.prototype = Object.extendsObject(OO1Base, {
    /*
        Ideally only used within the context of a workflow.
    */
    o : {},

    initialize: function(properties) 
    {	
        var self = this;
        //
        OO1Base.prototype.initialize.call(this, (function(o){            
            //  pass any defaults options.
            if (typeof properties != "undefined"){
                if (typeof properties == "object") {
                    for (var key in properties) o[key] = properties[key];                
                }  
            }
            return o;       
        })({
            // start a cart session
            "cart"      : this.create(),
            "item"      : null,
            //  store the master 'sc_req_item' sys_id       
            "order"     : null       
		}));  
    },

    /* 
        start the shopping cart process / catalog item process
    */
    create : function()
    {
        return this.o.cart = new Cart(GlideGuid.generate(null));       
    },

    /*
        add to product/item the cart.
        @id         : string - sys_id of the catalog item to add to the cart.
        @mappings   : object - object or mappings
        @object     : object - GlideRecord || Object        
    */
    item : function(id)
    {
        if (this.o.cart && typeof id != "undefined") {
            this.o.item = this.o.cart.addItem(id);          
        }
        return this.o.item;   
    },

    
    /*
        
    */
    variable : function(key, value)
    {
        if (this.o.cart && this.o.item) {
            this.o.cart.setVariable(this.o.item, key, value); 
        }
    },



    /*
        add to product/item the cart.
        @id         : string - sys_id of the catalog item to add to the cart.
        @mappings   : object - object or mappings
        @object     : object - GlideRecord || Object        
    */
    add : function(id, mappings, object)
    {
        //   setup the item, 
        if (this.item(id)) {
            if (typeof mappings != "undefined") {
                //  needs mapping or is it already mapped?
                var mapped = (typeof object != "undefined" ? this.map(mappings, object) : mappings);

                for (var field in mapped) {
                    this.variable(key, mapped[key]);
                }
            }
            return this;     
        }
        return null;
    },




    /*
        @retrieve : return the sc_req_item object?
    */
    order : function(retrieve)
    {
        if (this.o.cart) {
            var order = this.o.cart.placeOrder();
            if (order && order.hasOwnProperty("sys_id")) {
                
                this.o.order = String(order.sys_id);

                if (retrieve) {
                    var db = new GlideRecord("sc_req_item");
                    if (db.get(this.o.order)) {
                        return db;
                    }
                }
                return this.o.order;
            }
        }
        return false;
    },

    /*
        returns orderd items.

    */
    ordered : function(id)
    {
        id = id || this.o.order;

        //  get all the items in the request associated to this sc_req_item order.
    },

     type: 'OO1Cart'
});