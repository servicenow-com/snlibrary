var OO1APIExtendedModel = Class.create();
OO1APIExtendedModel.prototype = Object.extendsObject(OO1APIExtended, {
    
    o : {},
    
    initialize: function(properties) {
		OO1APIExtended.prototype.initialize.call(this, (function(o){    
            //  pass any defaults options.
            if (typeof properties == "object") {
                for (var key in properties) o[key] = properties[key];                
            }  
            return o;       
        })({
        	"endpoint" : "name"
        }));  	        
		//
		//this.OOAPI = new OO1APIExtended({"endpoint" : this.o.endpoint});
    },

	
	success : function(response, data, callback)
	{
		// validate the response
		if (typeof callback == "function") {
			callback(response, data);
		}	
	},


	failure : function(response, data, callback)
	{
		// validate the response
		if (typeof callback == "function") {
			callback(response, data);
		}
	},
	

	/*
		Use the API to send the request to AWS.
	*/
	send : function(data, success, failure)
	{
		var self = this;
		// call the API endpoint
        this.OOAPI.postAPIExtendedMethod(
			data, 
			//	success
			(function(data, response, request, api){
				self.success(response, data, success); 				    				
			}),
			// failure
			(function(data, response, request, api){
				self.failed(response, data, failure);  			
			})    		
		);
	},
	
	
	/*
		Payload data mapping against passed Glide object
		@gr : GlideRecord object
	*/
	data : function(gr)
	{
		return this.map({
			"sys_id" : "sys_id"			
		}, gr);
	},

    type: 'OO1APIExtendedModel'
});