var OO1API = Class.create();
OO1API.prototype = Object.extendsObject(OO1Base, {

    /*
        This class shouldn't really be used directly. Should be extended.

        Embedded Classes
        DB | sn_ws
    */
    
    //  decalared here to ensure that the scope for object works correctly.
    o : {},
    //  stores the SN REst API class
    SN_API : null,
    
    initialize : function(properties) 
    {  
        OO1Base.prototype.initialize.call(this, (function(o){            
            //  pass any defaults options.
            if (typeof properties == "object") {                
                //  lasy passing of options. See if additional properties other than 'options'
                //  allows to lazy pass only the 'options' to the API when being extended.
                var prefix = "option.";
                for (var key in properties) {
                    if (key.indexOf(".") !== -1 || o.hasOwnProperty(key)) {
                        prefix = "";
                        break;
                    }
                }
                for (var key in properties) o[(prefix + key)] = properties[key];                                    
            }              
            return o;       
        })({ 
            //  API Transaction table.
            "transaction" : {
                "table"   : "u_api_transaction",
                "mappings" : { 
                    "request" : {
                        "method"  : "u_method",
                        "headers" : "u_headers",
                        "url"     : "u_url",
                        "data"    : "u_data",
                        "name"    : "u_name"                        
                    },
                    "response" : {
                        "headers" : "u_response_headers",
                        "data"    : "u_response",                        
                        "status"  : "u_status",
                        "state"   : "u_state",                         
                        "message" : "u_response_message"
                    },
                    //  additional custom fields mapped during processing.
                    "fields" : {
                        "u_type" : "@outbound", // @inbound for REST, set in OOREST class
                        // incase enum values are ProperCase due to entering via SN UI.
                        // "u_method" : (function(d, k){
                            // return d[k].charAt(0).toUpperCase() + d[k].slice(1).toLowerCase();
                            // return d[k];
                        // }),
                        // "u_state" : (function(d, k){
                            // return d[k].charAt(0).toUpperCase() + d[k].slice(1).toLowerCase();
                            // return d[k];
                        // }),
                    },
                    //  used to increase the cache count, how many times it was retrieved.
                    "cache" : {
                        "retrieved" : "u_retrieved"
                    }
                }, 
                "sys_id" : null               
            },  
            "config" : {
                "library"  : "RESTMessageV2",
                "mappings" : {
                    //  Servicenow Class Wrapper mapping
                    "RESTMessageV2" : {
                        "method"     : "setHttpMethod",
                        "headers"    : "setRequestHeader",
                        "url"        : "setEndpoint",               
                        "data"       : "setRequestBody",
                        "attachment" : "setRequestBodyFromAttachment",
                        //  auth is dynamically checked on the passing of the 'profile' option
                        "auth"       : "setBasicAuth" // or setAuthenticationProfile                         
                    }                    
                },
                //  what a default request would be.
                //  this can be extended through using the properties.
                //  and used to prepopulate when endpoint is supplied.
                "request" : {
                    //  these can be added also
                    //  host    : null
                    //  segments : null
                    "method"  : "GET",
                    "headers" : {
                        //  default headers
                        "Accept"        : "application/json",
                        "Content-Type"  : "application/json",               
                    },
                    // string
                    "url"     : null,
                    //  object  
                    "data"    : null,
                    // callback function 
                    "success" : null,
                    // callback function
                    "failed"  : null 
                }                
            },
            "registry" : {                
                "start"     : null,
                "end"       : null,
                "retries"   : 0,
                //  what endpoint is being used?
                "endpoint"  : null                           
            },
            //  the request to be used.
            "request"  : null,
            "response" : null,
            //  API property register
            "option" : {
                //  API will run once, and then try another '2' more. Totaling 3 attempts
                "retries"   : 2,
                //  how long the api should wait before retrying Milliseconds
                "wait"      : 3000,     
                //  used to API rate calls. TO BE DONE.
                "rate"      : 0,           
                //  how offen is the cache refreshed? Milliseconds
                "cache"     : true,
                "refresh"   : 10000,
                //  Log into a transaction table. Cache will use this table.
                //  becareful turning this off at this level, as it will break other extend classes 
                //  if the transaction table is required.
                "log"       : true, 
                //  auth
                "auth"      : null, // 'basic' || 'OAuth2',
                "username"  : null,  // 
                "password"  : null,
                // profile name of the SN auth profile
                "profile"   : null,                 
                //
                "endpoint"  : null                
            }, 
            "status"    : {
                "0"   : "Unknown host. Host could not be resolved",
                "200" : "Success",
                "201" : "Success",
                "400" : "Bad request. Requested URL was not found or the URL contains incorrect segments",
                "401" : "Bad request. Data object is not valid",
                "403" : "Permission denied",
                "404" : "Not Found. Requested URL was not found on the server",
                "500" : "Connection error. Server did not respond",            
            },
            "endpoints" : null
        }));

        // Used for Outbound API calls.
        if (this.oo("config.library") == "RESTMessageV2") {
            // setup endpoint reference.
            if (this.oo("endpoints")) {
                //  if end point was manually set.
                if (this.oo("option.endpoint")) {
                    var ep = this.o.endpoints[this.o.option.endpoint];
                    if (ep) {
                        for(var option in this.o.option) {
                            if (ep.hasOwnProperty(option)) {
                                this.oo("option."+ option, ep[option])
                            }                    
                        }
                        this.oo("registry.endpoint", ep);                       
                    }
                }
            }        

            //  ensure that the correct library and mapped methid is being used for AUTH            
            if (this.oo("option.auth") && this.oo("option.profile")) {
                //  set the correct auth
                this.oo("config.mappings.RESTMessageV2.auth", "setAuthenticationProfile");
            }
            //  init the SN library class
            this.SN_API = new sn_ws[this.o.config.library]();            
        }
    },


    /*
        @request: object
    */
    get : function(request)
    {
        this.execute(request);
    },

    /*
        @request: object
    */
    post : function(request)
    {
        this.execute(this.JSON(request).merge({"method" : "POST"}));
    },

    patch : function(request)
    {
        this.execute(this.JSON(request).merge({"method" : "PATCH"}));
    },
    
    put : function(request)
    {
        this.execute(this.JSON(request).merge({"method" : "PUT"}));
    },

    /*
        Auth - Depending on the auth method.    
        @param1 : basic:username or auth type
        @param2 : basic:password or auth profile

        - built this way to be extended.
    */    
    auth : function(param1, param2)
    {
        this.SN_API[this.o.config.mappings[this.o.config.library].auth](param1, param2);

        return this;
    },
    

    /*
        @request : object
    */
    execute : function(request, retrying)
    {
        var self = this;
        //  reset anything.      
        if (typeof retrying == "undefined") {
            this.o.registry.retries = 0;
            this.o.registry.start = null;
            this.o.registry.end   = null;
        }
        //  set the request up
        this.o.request = (function(){
            var o = {};
            
            if (self.oo("config.request"))    o = self.JSON(self.oo("config.request")).clone();                        
            if (self.oo("registry.endpoint")) o = self.JSON(o).merge(self.oo("registry.endpoint"));                
            if (typeof request == "object")   o = self.JSON(o).merge(request);                
            
            //  tidy up URL
            var a = [];
            if (o.hasOwnProperty("host") && o.host) a.push(o.host[o.host.length - 1].indexOf("/") === 0 ? o.host.substring(0, o.host.length - 1) : o.host);                     
            if (o.hasOwnProperty("segments") && o.segments) {
                var s = o.segments.indexOf("/") === 0 ? o.segments.substring(1) : o.segments;
                v =     (s[s.length - 1].indexOf("/") === 0 ? s.substring(0, s.length - 1) : s);
                a.push(v); 
                //  remove
                delete o.segments;               
            }
            if (o.hasOwnProperty("url") && o.url) a.push(o.url.indexOf("/") === 0 ? o.url.substring(1) : o.url);                
            
            o.url = a.join("/");
            return o;    
        })();

        //  init the SN library class
        //  ensure that the correct library and mapped methid is being used for AUTH
        if (this.oo("config.library") == "RESTMessageV2") {
            this.SN_API = new sn_ws[this.o.config.library]();                
        }
        //  
        if (!this.hasOwnProperty("SN_API")) return false;
        
        //  setup the class library to use
        //  setup the library API class mapping
        var map = this.o.config.mappings[this.o.config.library];        
        for (var key in map) {            
            if (this.oo("request."+ key)) {
                switch (key) {
                    case "headers" : 
                        //
                        if (typeof this['headers'] == "function") {
                            this.headers();    
                            continue;
                        }   
                        //  default fall back
                        for(var header in this.o.request[key]){
                            this.SN_API[map[key]](header, this.o.request[key][header]);    
                        }                                        
                    break;
                    case "data" :
                        this.SN_API[map[key]](typeof this.o.request[key] == "object" ? JSON.stringify(this.o.request[key]) : this.o.request[key]);                        
                    break;
                    case "auth" :
                        if (this.oo("option.auth")) {
                            //  SN token or auth profile is to be used.
                            if (this.oo("option.profile")) {                                
                                this.auth(this.oo("option.auth"), this.oo("option.profile"));
                            }else{
                                this.auth(this.oo("option.username"), this.oo("option.password"));
                            }
                        }                        
                    break;
                    default :
                        if (typeof this.SN_API[map[key]] == "function") {
                            this.SN_API[map[key]](this.o.request[key]);    
                        }                       
                    break;
                }   
            }
        }


        //  Start the request
        if (this.oo("registry.retries") === 0) {
            this.oo("registry.start", new GlideDateTime().getNumericValue());
        }
        
        if (this.oo("option.cache") && (typeof this['cache'] == 'function')) {
            var cache = this.cache(request);
            if (cache) {
                //
                var response = this.cache();                
                if (response) {
                    //
                    this.o.response = response;

                    if (request.hasOwnProperty("success") && typeof request.success == "function") {
                        return request.success(response.data, this);              
                    }
                    return ;
                }
            }                    
        }

        // execute the request
        var response = this.SN_API.execute();  
        //
        this.oo("registry.end", new GlideDateTime().getNumericValue());                        
        //  retrieve the response        
        this.o.response = {
            "headers" : response.getHeaders(), // object
            "status"  : response.getStatusCode(),   // number
            "data"    : (function(){
                var v = response.getBody();  
                //  return JSON.              
                return (typeof v == "string" && (v.indexOf("{") === 0 || v.indexOf("[") === 0) ? JSON.parse(v) : v);                                
            })(),
            "start"     : this.oo("registry.start"),           
            "end"       : this.oo("registry.end"),
            "retries"   : this.oo("registry.retries"),
        };                
        
        // any custom messages? 
        this.o.response.message = (function(status){            
            if (self.oo("registry.message."+ status)) {
                var msg = self.oo("registry.message."+ status);
                if (typeof msg == "function"){
                    return msg(this.o.response, self);
                }
                return msg;
            }
            // any default Status messages?
            if (self.oo("status."+ status)) {
                return self.oo("status."+ status);                
            }
            return null;
        })(response.getStatusCode().toString());

        //
        if (parseInt(this.o.response.status) >= 200 && parseInt(this.o.response.status) < 299) {
            
            this.o.response.state = "success";

            if (this.oo("option.log") == true) {
                this.o.response.transaction = this.transaction();
            }
            //
            if (typeof this.o.request.success == "function") {
                return this.o.request.success(this.o.response.data, this.o.response, this.o.request, this);              
            }
        }else{
            if (this.o.registry.retries < (parseInt(this.o.option.retries))) {
                //  
                this.o.registry.retries++;
                //  wait before retrying the api request
                if (this.oo("option.wait")) {
                    gs.sleep(this.o.option.wait);
                }
                //   try again.
                return this.execute(request, true);
            }
            //
            this.o.response.state = "failed";       

            //  LOG
            if (this.oo("option.log") == true) {
                this.o.response.transaction = this.transaction();
            }

            if (typeof this.o.request.failed == "function") {  
                this.o.request.failed(this.o.response.data, this.o.response, this.o.request, this);                              
            }
        } 
    },

    //  retrieves the cache from the transaction table.
    cache : function(request)
    {
        var self = this;
        var tbl = this.oo("transaction.table");
        if (tbl) {
            //
            var db  = new GlideRecord(tbl); 
            if (db.hasOwnProperty("sys_meta")) {
                var m = this.oo("transaction.mappings.request");
                var a = [this.oo("transaction.mappings.response.state") +"=success"];
                for (var k in m) {
                    if (request.hasOwnProperty(k)) {
                        a.push(m[k] +"="+ (function(){
                            if (typeof request[k] == "object") {
                                return JSON.stringify(request[k]);
                            }
                            return request[k];
                        })());    
                    }                    
                }
                //  set refresh rate
                if (this.oo("option.refresh")){
                    a.push((function(milliseconds){
                        var dt = new GlideDateTime();
                        dt.add(-milliseconds);
                        return "sys_created_on>="+ dt;
                    })(parseInt(this.oo("option.refresh"))));
                }
                a.push("ORDERBYDESCsys_created_on");
                //
                db.addEncodedQuery(a.join("^"));
                db.setLimit(1);
                db.query();
                //
                while (db.next()) {
                    //  increase the cache count.
                    var key = this.oo("transaction.mappings.cache.retrieved");
                    if (key) {
                        db[key] = parseInt(db[key].getValue()) + 1;
                        db.update()
                    }
                    //  ensture that the data field is converted to JSON if it's an object
                    var o = {
                        "headers" : (function(db){
                            var v = db[self.oo('transaction.mappings.response.headers')].getValue();
                            return (typeof v == "string" && (v.indexOf("{") === 0 || v.indexOf("[") === 0) ? JSON.parse(v) : v);                            
                        }),  
                        "data"   : (function(db){
                            var v = db[self.oo('transaction.mappings.response.data')].getValue();
                            return (typeof v == "string" && (v.indexOf("{") === 0 || v.indexOf("[") === 0) ? JSON.parse(v) : v);                            
                        }),
                        "status"  : (function(db){
                            return parseInt(db[self.oo('transaction.mappings.response.status')].getValue()); 
                        }),                        
                        "cache"   : true,
                        "start"   : "@"+ this.oo("registry.start"),
                        "end"     : "@"+ this.oo("registry.end", new GlideDateTime().getNumericValue())
                    } 
                    //
                    return this.map(this.JSON(this.o.transaction.mappings.response).merge(o), db);                    
                }
            }
        }
        return null;
    },

    /*
        API Transaction log table
        @data : object - Additional DB fields.
    */
    transaction : function(data)
    {
        var tbl = this.oo("transaction.table");
        if (tbl) {
            //  validates that the table                      
            var db  = new GlideRecord(tbl); 
            if (db.hasOwnProperty("sys_meta")) {
                //  swap the mapping object keys values around for mapping
                data = this.map((function(map){
                    var o = {};
                    for (var k in map) o[map[k]] = k;
                    return o;
                })(this.o.transaction.mappings.request), this.o.request, data);

                //
                if (this.o.response) {
                    //  swap the mapping object keys values around for mapping
                    data = this.map((function(map){
                        var o = {};
                        for (var k in map) o[map[k]] = k;
                        return o;
                    })(this.o.transaction.mappings.response), this.o.response, data);
                }                
                //
                data = this.map(this.o.transaction.mappings.fields, data, data);
                //                
                var db = new OO1DB(tbl, false).insert(data);
                if (db && db.hasOwnProperty("sys_id")) {
                    return this.oo("transaction.sys_id", db.sys_id);
                }
            }    
        }   
        return null;     
    },

    test : function()
    {
        var self = this;
        this.get({
            "name"    : "API Test Outbound",
            "url"     : "https://httpbin.org/get",
            // "url"     : "get",
            "success" : (function(response){
                self.info(response)
            }),
            "failed" : (function(response){
                self.info(response)
            })
        })        
    },

    type : 'OO1API'    
});